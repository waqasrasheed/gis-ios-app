Pod::Spec.new do |spec|
  spec.name         = "GISFramework"
  spec.version      = "1.0"
  spec.summary      = "A small framework to draw a layer on Google Map in swift"
  spec.description  = <<-DESC
                    GISFramework is a small and lightweight Swift framework that allows to draw a layer on Google Map
                    DESC

  spec.homepage     = "https://gitlab.com/waqasrasheed/gis-ios-app"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author       = { "Waqas Rasheed" => "raoowaqas@gmail.com" }
  spec.platform     = :ios, "11.0"
  
  spec.source       = { :git => "https://gitlab.com/waqasrasheed/gis-ios-app.git", :tag => "v0.7"}

  #spec.source       = { :git => "https://gitlab.com/waqasrasheed/gis-ios-app.git"}
  #spec.source       = { :git => "https://gitlab.com/waqasrasheed/gis-ios-app.git", :tag => "v0.7"}
  #spec.source       = { :git => "https://gitlab.com/waqasrasheed/gis-ios-app.git", :branch => "branchName"}
  #spec.source       = { :git => "https://gitlab.com/waqasrasheed/gis-ios-app.git", :commit => "0f50b1c45"}
  #spec.source       = { :path => "/Volumes/TargetOFS/WMS WorkSpace/Project/GISLayerApp"}
  
 spec.swift_version = "5.0"
 spec.source_files = "GISFramework/GISFramework/**/*.{swift}"
 spec.resources = "GISFramework/GISFramework/**/*.{png,jpeg,jpg,storyboard,xib,xcassets}"
 
 spec.static_framework = true
 spec.framework    = "UIKit"
 spec.dependency 'GoogleMaps'
 spec.dependency 'Google-Maps-iOS-Utils'
 spec.dependency 'Alamofire', '~> 5.0.0-rc.3'
 
 spec.dependency 'SwiftyJSON'
 spec.dependency 'MBProgressHUD', '~> 1.2.0'
 spec.dependency 'IQKeyboardManagerSwift'
 spec.dependency 'SDWebImage'

end