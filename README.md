# GISFramework - IOS

## Description
GISFramework is a small and lightweight Swift framework that allows to draw a layer on Google Map

## Requirements

* iOS 11.0+
* CocoaPods

## CocoaPods Installation

 In your `Podfile`:

 ```ruby
 use_frameworks!

 target 'TARGET_NAME' do
     pod 'GISFramework' , :git => 'https://gitlab.com/waqasrasheed/gis-ios-app.git' , :tag => 'v0.7'
 end
``` 
Replace `TARGET_NAME` and then, in the `Podfile` directory, type:

```bash
$ pod install
```

## Usage

First you need to configure with your server path

Server path: map.demo-aws.meeraspace.com

Call this function to set the serverPath

GISFramework.configure(gisDomain: "demo-aws.meeraspace.com", gisSubDomain: "map")

#### Initialize the controller to show the map listing

```swift
import GISFramework

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

       GISFramework.configure(gisDomain: "demo-aws.meeraspace.com", gisSubDomain: "map")

       let controller = GISMainTabController()
       navigationController.setViewControllers([controller], animated: true)
       UIApplication.shared.windows.first?.rootViewController = navigationController
       UIApplication.shared.windows.first?.makeKeyAndVisible()

      return true
    }
}
```
