//
//  AppDelegate.swift
//  GISLayerApp
//
//  Created by Waqas Rasheed on 23/02/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit
import GISFramework

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    static var shared: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        print("[AppDelegate]: number of windows \(UIApplication.shared.windows.count)")
        
        launchApplication()
        
        return true
    }

}

// MARK: - Utility Methods
extension AppDelegate {
    
    func launchApplication() {
        
        GISFramework.configure(gisDomain: domainName, gisSubDomain: "map")
        
        let navigationController = UINavigationController()
        
        //let controller = GISMainTabController()
        let controller = GISGenralViewController()
        
        navigationController.setViewControllers([controller], animated: true)
        navigationController.isNavigationBarHidden = true

        
        if #available(iOS 13, *) {
            UIApplication.shared.windows.first?.rootViewController = navigationController
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        } else {
            
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = navigationController
            window?.makeKeyAndVisible()
            
        }
        
        
    }
    
    func launchApplicationWithLoginModuel() {
        
        GISFramework.configure(gisDomain: domainName, gisSubDomain: "was")
        
        let navigationController = UINavigationController()
        
        if MyDefaults.shared.user.id.count > 0 {
            
            //let controller = GISMainTabController()
            let controller = GISGenralViewController()
            
            navigationController.setViewControllers([controller], animated: true)
            
        } else {
            let controller = LoginController(nibName: LoginController.name, bundle: nil)
            navigationController.setViewControllers([controller], animated: true)
        }
        
        
        navigationController.isNavigationBarHidden = true

        
        if #available(iOS 13, *) {
            UIApplication.shared.windows.first?.rootViewController = navigationController
            UIApplication.shared.windows.first?.makeKeyAndVisible()
        } else {
            
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.rootViewController = navigationController
            window?.makeKeyAndVisible()
            
        }
        
        
    }
}
