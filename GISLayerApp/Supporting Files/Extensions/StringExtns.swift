//
//  StringExtns.swift
//  SSO Login
//
//  Created by Zuhair Hussain on 12/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import UIKit


extension String {
    var bundleImage: UIImage? {
        return UIImage(named: self)
    }
}
