//
//  UIElementsExt.swift
//  Carzly
//
//  Created by Zuhair Hussain on 11/06/2019.
//  Copyright © 2019 Zuhair Hussain. All rights reserved.
//

import UIKit

extension UIScreen {
    static var width: CGFloat {
        return UIScreen.main.bounds.width
    }
    static var height: CGFloat {
        return UIScreen.main.bounds.height
    }
}

extension UIViewController {
    @IBAction @objc func didTapBackButton() {
        navigationController?.popViewController(animated: true)
    }
    @IBAction @objc func didTapCloseButton() {
        self.dismiss(animated: true, completion: nil)
    }
    static var name: String {
        return String(describing: self)
    }
    
    @IBAction func willBeImplementedLater() {
        let alert = UIAlertController(title: nil, message: "Will be implemented later", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}



extension Array {
    func object(at index: Int) -> Element? {
        if index < self.count && index >= 0 {
            return self[index]
        }
        return nil
    }
    mutating func replace(_ element: Element, at index: Int) {
        if index < self.count && index >= 0 {
            self[index] = element
        }
    }
}

extension UIColor {
    
    convenience init(hexString:String) {
        let hexString:NSString =
            hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as NSString
        
        //hexString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let scanner            = Scanner(string: hexString as String)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
    
}

extension UICollectionView {
    func registerCell(ofType type: Any, withReuseIdentifier identifier: String) {
        let className = String(describing: type)
        let nib = UINib(nibName: className, bundle: Bundle.main)
        self.register(nib, forCellWithReuseIdentifier: identifier)
    }
    func registerHeader(ofType type: Any, withReuseIdentifier identifier: String) {
        let className = String(describing: type)
        self.register(UINib(nibName: className, bundle: Bundle.main), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: identifier)
    }
}

extension UICollectionViewCell {
    static var identifier: String {
        return "k\(String(describing: self))"
    }
}

extension UITableView {
    
    func addRefreshControl(_ target: Any, selector: Selector) {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(target, action: selector, for: .valueChanged)
        refreshControl.backgroundColor = UIColor.clear
        self.refreshControl = refreshControl
    }
    func registerCell(ofType type: Any, withReuseIdentifier identifier: String) {
        let className = String(describing: type)
        let nib = UINib(nibName: className, bundle: Bundle.main)
        self.register(nib, forCellReuseIdentifier: identifier)
    }
}


extension UITableViewCell {
    static var identifier: String {
        return "k\(String(describing: self))"
    }
}

extension Array where Element: UIView {
    func index(of view: UIView) -> Int? {
        for (index, value) in self.enumerated() {
            if value == view {
                return index
            }
        }
        return nil
    }
}

extension UIImageView {
    override open func awakeFromNib() {
        super.awakeFromNib()
        tintColorDidChange()
    }
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
}



extension UIView {
    
    private struct AssociatedKeys {
        static var shaddow = "shaddowKey"
    }
    
    func layoutIfNeeded(animated: Bool, completion: (() -> Void)? = nil) {
        if animated {
            UIView.animate(withDuration: 0.3, animations: {
                self.layoutIfNeeded()
            }) { (_) in
                completion?()
            }
        } else {
            self.layoutIfNeeded()
        }
    }
    
    @IBInspectable var showShaddow : Bool {
        get {
            guard let number = objc_getAssociatedObject(self, &AssociatedKeys.shaddow) as? NSNumber else {
                return true
            }
            return number.boolValue
        }
        
        set {
            
            if newValue {
                DispatchQueue.main.async {
                    self.layer.masksToBounds = false
                    self.layer.shadowColor = UIColor(red: 140 / 255, green: 140 / 255, blue: 170 / 255, alpha: 1).cgColor
                    self.layer.shadowOpacity = 0.4
                    self.layer.shadowOffset = CGSize(width: 0, height: 1)
                    self.layer.shadowRadius = 6
                    self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
                    self.layer.shouldRasterize = true
                    self.layer.rasterizationScale = UIScreen.main.scale
                }
            }
            
            objc_setAssociatedObject(self,&AssociatedKeys.shaddow,NSNumber(value: newValue),objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        }
    }
    
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        get {
            return UIColor(cgColor: layer.borderColor!)
        }
        set {
            layer.borderColor = newValue?.cgColor
        }
    }
    
    func hide() {
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }) { (_) in
            self.isHidden = true
            self.alpha = 1
        }
    }
    func show() {
        if self.isHidden {
            self.alpha = 0
            self.isHidden = false
            UIView.animate(withDuration: 0.3) {
                self.alpha = 1
            }
        }
    }
    func removeAllSubViews() {
        for v in self.subviews {
            v.removeFromSuperview()
        }
    }
    
    
    
    func animate(with animationType: ViewAnimationType) { 
        switch animationType {
        case .shake:
            self.shake()
            break
        case .pulse:
            self.pulse()
            break
        }
    }
    
    
    private func shake() {
        self.transform = CGAffineTransform(translationX: 16, y: 0)
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.transform = CGAffineTransform.identity
        }, completion: nil)
    }
    
    private func pulse() {
        self.alpha = 0.3
        self.layer.transform = CATransform3DMakeScale(0.8, 1, 0.8)
        
        
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1.0, options: .curveEaseInOut, animations: {
            self.alpha = 1
            self.layer.transform = CATransform3DScale(CATransform3DIdentity, 1, 1, 1)
        })
    }
    
}

extension UIAlertController {
    func show() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            let win = UIWindow(frame: UIScreen.main.bounds)
            let vc = UIViewController()
            vc.view.backgroundColor = .clear
            win.rootViewController = vc
            win.windowLevel = UIWindow.Level.alert + 1
            win.makeKeyAndVisible()
            vc.present(self, animated: true, completion: nil)
        }
    }
}

enum ViewAnimationType: Int {
    case shake = 0
    case pulse = 1
}
