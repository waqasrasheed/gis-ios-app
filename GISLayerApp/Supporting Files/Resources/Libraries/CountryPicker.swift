//
//  CountryPicker.swift
//  SSO Login
//
//  Created by Zuhair Hussain on 12/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import CountryPickerView

class CountryPicker {
    
    let countryPicker = CountryPickerView()
    var completion: ((_ country: CPCountry) -> Void)?
    
    static let shared = CountryPicker()
    
    func currentCountry() -> CPCountry {
        
        let countryPicker = CountryPickerView()
        let currentLocale = NSLocale.current as NSLocale
        if let countryCode = currentLocale.object(forKey: .countryCode) as? String {
            countryPicker.setCountryByCode(countryCode)
        }
        return CPCountry(countryPicker.selectedCountry)
    }
    
    
    private init() {
        configure()
    }
    
    
    func configure() {
        let currentLocale = NSLocale.current as NSLocale
        if let countryCode = currentLocale.object(forKey: .countryCode) as? String {
            countryPicker.setCountryByCode(countryCode)
        }
        countryPicker.delegate = self
        
    }
    
    
    func show(from controller: UIViewController, completion: @escaping (_ country: CPCountry) -> Void) {
        self.completion = completion
        countryPicker.showCountriesList(from: controller)
    }
    
    
    
}

// MARK: - CountryPicker Delegate
extension CountryPicker: CountryPickerViewDelegate {
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        let c = CPCountry(country)
        completion?(c)
    }
}


struct CPCountry {
    var name = ""
    var code = ""
    var phoneCode = ""
    var localizedName = ""
    var flag: UIImage!
    
    init() {}
    init(_ country: Country) {
        self.name = country.name
        self.code = country.code
        self.phoneCode = country.phoneCode
        self.localizedName = country.localizedName() ?? name
        self.flag = country.flag
    }
    
}
