//
//  Enums.swift
//  SSO Login
//
//  Created by Zuhair Hussain on 12/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import Foundation

enum Status: Int {
    case failure = 0
    case success = 1
}
