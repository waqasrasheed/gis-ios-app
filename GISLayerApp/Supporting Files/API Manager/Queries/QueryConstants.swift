//
//  Queries.swift
//  SSO Login
//
//  Created by Zuhair Hussain on 12/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import Foundation


let Q_UserInfo =  "query getUserInfo { me { id username fullName email gender phone password nationality photo dateOfBirth streetAddress activated createdDate city { id cityName } province { id provinceName } country { id countryName countryCode } postalCode userRoles { userRoles { id role { id name } } } } checkUserProfile { userRoles { role status restaurant { id personalFullName personalPhone personalEmail businessName description createdDate phone photo star createdBy { username fullName id } starCount completeAddr foodRestaurantCuisines { totalCount foodRestaurantCuisines { id name cuisine { id name icon } } } minimumOrderVal deliveryCharges invitationCode foodRestaurantSchedules { foodRestaurantSchedules { id dayOfWeek openTime closeTime isClosed } } location { id name streetAddress city { id cityName } isHeadquarter phone latitude longitude country { id countryName } province { id provinceName } } foodRestaurantCovers { foodRestaurantCovers { id uRL } } } supplier { id businessName businessType registrationNumber businessMobile businessEmail description rating photo createdBy { id fullName } businessHours { id isClosed dayOfWeek openTime closeTime } shopAddress { id name streetAddress city { id cityName } isHeadquarter phone latitude longitude country { id countryName } province { id provinceName } } } serviceProvider { id name description createdDate isActive photo phone providerServices { totalCount } location { id latitude longitude streetAddress city { id cityName } country { id countryName } } createdBy { id fullName } spBusinessTimes { spBusinessTimes { id sp { id name } dayOfWeek openTime closeTime isClosed } } spGalleries(limit: 200, offset: 0) { spGalleries { id name picURL } } } promoter { id } courier { id name } } } } ";
