//
//  GraphQLRequest.swift
//  SSO Login
//
//  Created by Zuhair Hussain on 12/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import SwiftyJSON


public typealias GraphQLResponseHandler = (_ result:JSON?, _ error:ErrorModel?) ->Void

class GraphQLManager {
    
    static let shared = GraphQLManager()
    
    private let manager = APIManagerBase()
    
    
    func request(query: String, parameters inputParameters: [String: Any] = [:], completion: @escaping GraphQLResponseHandler) {
        
        var parameters: [String: Any] = ["query": query]
        if inputParameters.count > 0 {
            parameters[""] = inputParameters
        }
        
        manager.postRequest(url: baseUrl, parameters: parameters) { (status, response) in
            
            if status == .success {
                if let res = response {
                    
                    let jsonVal = JSON(res)
                    let parseJson = jsonVal.rawValue as! [String:Any]
                    let graphQLError = parseJson["errors"] as? [[String:Any]]
                    
                    
                    if  let _ = graphQLError {
                        if (graphQLError!.count > 0) {
                            let msgDic = graphQLError![0];
                            if let msg = msgDic["message"] as? String {
                                completion(nil,ErrorModel(errorTitle: "Xeena", errorDescp: msg, error: nil, errorCode: 0))
                            } else {
                                completion(nil,ErrorModel(errorTitle: "Xeena", errorDescp: "Unknown Error", error: nil, errorCode: 0))
                            }
                            return
                        }
                    }
                    
                    
                    if let _ = jsonVal.rawValue as? [String:Any] {
                        DispatchQueue.main.async {
                            completion(jsonVal, nil)
                        }
                    } else {
                        completion(nil,ErrorModel(errorTitle: "Json Parsin Error", errorDescp: "Json parsing issue", error: nil, errorCode:0))
                    }
                }
                
                
            } else {
                completion(nil, ErrorModel(errorTitle: "Xeena", errorDescp: status.message, error: nil))
            }
            
        }
    }
    
}
