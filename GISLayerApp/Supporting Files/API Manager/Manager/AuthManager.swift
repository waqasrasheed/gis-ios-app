//
//  AuthManager.swift
//  SSO Login
//
//  Created by Zuhair Hussain on 12/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import SwiftyJSON

class AuthManager {
    
    private let graphQLManager = GraphQLManager()
    
    
    
    func login(_ username: String, password: String, completion: @escaping (_ user: UserModel, _ error: ErrorModel?) -> Void) {
        preparingOth2WithPassword(username: username, password: password) { (token, error) in
            if let e = error {
                completion(UserModel(), e)
            } else {
                Keychain.shared.accessToken = token.accessToken
                Keychain.shared.refreshToken = token.refreshToken
                self.getUserProfile(completion: completion)
            }
        }
    }
    
    func getUserProfile(completion: @escaping (_ user: UserModel, _ error: ErrorModel?) -> Void) {
        graphQLManager.request(query: Q_UserInfo) { (json, error) in
            
            if error == nil && json != nil {
                let user = UserModel(json: json!["data"]["me"])
                completion(user, nil)
            } else {
                completion(UserModel(), error!)
            }
            
        }
    }
    
    
    
    private func preparingOth2WithPassword(username:String, password: String,completion:@escaping (_ token: (accessToken:String, refreshToken: String), _ error: ErrorModel?) ->Void) {
  
        let _ = oauthswift.authorize(username: username, password: password, scope: authScope) { result in
            switch (result) {
                
            case .success(let (_, response, _)):
                if let jsonResponce = response?.data {
                    
                    do {
                        let jsonResult = try JSONSerialization.jsonObject(with: jsonResponce, options: JSONSerialization.ReadingOptions.allowFragments)
                        
                        let parseJson = jsonResult as! [String:Any]
                        
                        
                        var refreshToken = ""
                        if let accessToken = parseJson["access_token"] as? String {
                            
                            
                            if let refresh_token = parseJson["refresh_token"] as? String {
                                refreshToken = refresh_token
                            }
                            
                            
                            completion((accessToken: accessToken, refreshToken: refreshToken), nil)
                            
                        } else {
                            completion((accessToken: "", refreshToken: ""), ErrorModel(errorTitle: "Xeena", errorDescp: "Auth token not found", error: nil))
                        }
                        
                    } catch {
                        completion((accessToken: "", refreshToken: ""), ErrorModel(errorTitle: "Xeena", errorDescp: error.localizedDescription, error: nil))
                    }
                    
                } else {
                    completion((accessToken: "", refreshToken: ""), ErrorModel(errorTitle: "Xeena", errorDescp: "Server not responding", error: nil))
                }
                break;
            case .failure(let error):
                let errorDic = error.errorUserInfo;
                let errorCustom = self.outhErrorHandler(dic: errorDic);
                
                if errorCustom.code == 401 {
                    completion((accessToken: "", refreshToken: ""), ErrorModel(errorTitle: "Xeena", errorDescp:  errorCustom.errorStr, error: nil, errorCode: 401))
                } else if errorCustom.code == 400 {
                    completion((accessToken: "", refreshToken: ""), ErrorModel(errorTitle: "Xeena", errorDescp:  errorCustom.errorStr, error: nil, errorCode: 400))
                } else {
                    completion((accessToken: "", refreshToken: ""), ErrorModel(errorTitle: "Xeena", errorDescp: error.localizedDescription, error: nil))
                }
                break;
                
            }
        }
    }
    
    
    private func outhErrorHandler(dic: [String:Any]?) -> (code:Int,errorStr:String) {
        if let _ = dic {
            if let m = dic!["error"] as? NSError {
                print(m.localizedDescription)
                print(m.code);
                return (code:m.code,errorStr:m.localizedDescription)
            }
        }
        return (code:0,errorStr:"Unknown error");
    }
    
}
