//
//  ErrorModel.swift
//  Qais Social
//
//  Created by Muhammad Arslan Khalid on 22/10/2018.
//  Copyright © 2018 Target OFS. All rights reserved.
//

import Foundation
public struct ErrorModel {
    
    var errorTitle:String?
    var errorDescp:String?
    var error:Error?
    var errorCode:Int!
    var errorDic: [String:Any]?
    
    
    init(errorTitle:String, errorDescp:String, error:Error?, errorCode:Int = 0, errorDic: [String:Any]? = nil) {
        self.errorTitle = errorTitle
        self.errorDescp = errorDescp
        self.error = error
        self.errorCode = errorCode
        self.errorDic = errorDic
    }
}
