//
//  APIConstants.swift
//  SSO Login
//
//  Created by Zuhair Hussain on 12/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import GISFramework

//var domainName = "dev.meeraspace.com"
var domainName = "demo-aws.meeraspace.com" //92 3350011220

var SSO_URL: String {
    return "https://sso.\(domainName)" // demo-aws sso
}

//var SSO_URL_GIS_LAYER: String {
//    return "https://sso.\(domainName)" // demo-aws sso
//    
//}

var oauthswift = OAuth2Swift(
    consumerKey: "mobile-app", //meera-dx         // [1] Enter google app settings
    consumerSecret: "ZXhhbXBsZS1hcHAtc2VjcmV1",        // No secret required
    authorizeUrl: SSO_URL + "/auth",
    accessTokenUrl: SSO_URL + "/token",
    responseType: "code"
)

let authScope = "openid email mobile groups profile offline_access"

var baseUrl: URL {
    return URL(string: "https://zeena.\(domainName)/graphql")!
}
