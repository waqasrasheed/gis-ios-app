//
//  APIManagerBase.swift
//  Carzly
//
//  Created by Zuhair Hussain on 11/06/2019.
//  Copyright © 2019 Zuhair Hussain. All rights reserved.
//

import UIKit

typealias APIResponseClousure = (_ status: APIStatus, _ response: Any?) -> Void

class APIManagerBase {
    var isNetworkReachable: Bool {
//        guard let reachability = Reachability.forInternetConnection() else { return true }
        return true
    }
    var headers: [String: String] {
        var h = ["Content-Type": "application/json"]
        if let token = Keychain.shared.accessToken {
            h["Authorization"] = "Bearer \(token)"
        }
        return h
    }
}



// MARK: - HTTP Methods
extension APIManagerBase {
    
    class func request(withImages url:URL, token : String?, headers:[String:String]?, parameters: [String:Any]?,imageNames : [String], images:[Data], completion: @escaping(Any?, Error?, Bool)->Void) {
        
        
        // generate boundary string using a unique per-app string
        let boundary = UUID().uuidString
        
        let config = URLSessionConfiguration.default
        let session = URLSession(configuration: config)
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        if headers != nil{
            print("\n\nHeaders :-------------- ",headers as Any,"\n\n --------------: Headers")
            for (key, value) in headers! {
                request.setValue(value, forHTTPHeaderField: key)
                
            }
        }
        
        // Set Content-Type Header to multipart/form-data, this is equivalent to submitting form data with file upload in a web browser
        // And the boundary is also set here
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        var data = Data()
        if parameters != nil{
            for(key, value) in parameters!{
                // Add the reqtype field and its value to the raw http request data
                data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
                data.append("Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n".data(using: .utf8)!)
                data.append("\(value)".data(using: .utf8)!)
            }
        }
        for (index,imageData) in images.enumerated() {
            // Add the image data to the raw http request data
            data.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
            data.append("Content-Disposition: form-data; name=\"\(imageNames[index])\"; filename=\"\(imageNames[index])\"\r\n".data(using: .utf8)!)
            data.append("Content-Type: image/jpg\r\n\r\n".data(using: .utf8)!)
            data.append(imageData)
        }
        
        // End the raw http request data, note that there is 2 extra dash ("-") at the end, this is to indicate the end of the data
        data.append("\r\n--\(boundary)--\r\n".data(using: .utf8)!)
        
        // Send a POST request to the URL, with the data we created earlier
        session.uploadTask(with: request, from: data, completionHandler: { data, response, error in
            
            if let checkResponse = response as? HTTPURLResponse{
                if checkResponse.statusCode == 200{
                    guard let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: [JSONSerialization.ReadingOptions.allowFragments]) else {
                        completion(nil, error, false)
                        return
                    }
                    let jsonString = String(data: data, encoding: .utf8)!
                    print("\n\n---------------------------\n\n"+jsonString+"\n\n---------------------------\n\n")
                    print(json)
                    completion(json, nil, true)
                }else{
                    guard let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []) else {
                        completion(nil, error, false)
                        return
                    }
                    let jsonString = String(data: data, encoding: .utf8)!
                    print("\n\n---------------------------\n\n"+jsonString+"\n\n---------------------------\n\n")
                    print(json)
                    completion(json, nil, false)
                }
            }else{
                guard let data = data, let json = try? JSONSerialization.jsonObject(with: data, options: []) else {
                    completion(nil, error, false)
                    return
                }
                completion(json, nil, false)
            }
            
        }).resume()
        
    }
    
    // GET Request
    func getRequest(url: URL, completion: @escaping APIResponseClousure) {
        
        if !isNetworkReachable {
            return completion(APIStatus.noNetwork, nil)
        }
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 90
        configuration.timeoutIntervalForResource = 90
        
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        session.dataTask(with: url) {(data, response, error) in
            
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                if error == nil && data != nil {
                    if let json = data!.json {
                        completion(APIStatus.success, json)
                    } else {
                        completion(APIStatus.invalidResponse, nil)
                    }
                } else {
                    let status = APIStatus.failureWith(message: error!.localizedDescription, code: 404)
                    completion(status, nil)
                }
            }
            
        }.resume()
    }
    
    // POST Request
    func postRequest(url: URL, parameters: [String: Any], completion: @escaping APIResponseClousure) {
        if !isNetworkReachable {
            return completion(APIStatus.noNetwork, nil)
        }
        
        
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 90
        configuration.timeoutIntervalForResource = 90
        
        let body = parameters.data
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.httpBody = body
        
        let h = headers
        for (key, value) in h {
            request.addValue(value, forHTTPHeaderField: key)
        }
        
        let session = URLSession(configuration: configuration, delegate: nil, delegateQueue: nil)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        session.dataTask(with: request) { (data, response, error) in
            
            DispatchQueue.main.async {
                UIApplication.shared.isNetworkActivityIndicatorVisible = false
                
                if error == nil && data != nil {
                    if let json = data!.json {
                        completion(APIStatus.success, json)
                    } else {
                        completion(APIStatus.invalidResponse, nil)
                    }
                } else {
                    let status = APIStatus.failureWith(message: error!.localizedDescription, code: 404)
                    completion(status, nil)
                    
                    
                }
            }
            
            }.resume()
    }
    
    func postRequestWithMultipart(url: URL, parameters: [String: Any], completion: @escaping APIResponseClousure) {
        
        
        if !isNetworkReachable {
            return completion(APIStatus.noNetwork, nil)
        }
        
        
        DispatchQueue.main.async {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
        
        
        
        DispatchQueue.global(qos: .background).async {
            
            let boundary = self.boundary
            let body = self.httpBody(with: parameters, boundary: boundary)
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            //        request.addValue("Client-ID f65203f7020dddc", forHTTPHeaderField: "Authorization")
            
            let h = self.headers
            for (key, value) in h {
                request.addValue(value, forHTTPHeaderField: key)
            }
            
            request.httpBody = body
            request.setValue("\(body.count)", forHTTPHeaderField:"Content-Length")
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                
                DispatchQueue.main.async {
                    UIApplication.shared.isNetworkActivityIndicatorVisible = false
                    
                    if error == nil && data != nil {
                        if let json = data!.json {
                            completion(APIStatus.success, json)
                        } else {
                            completion(APIStatus.invalidResponse, nil)
                        }
                    } else {
                        let status = APIStatus.failureWith(message: error!.localizedDescription, code: 404)
                        completion(status, nil)
                    }
                }
                
                }.resume()
        }
        
    }
}





// MARK: - Utility Methods
extension APIManagerBase {
    
    func httpBody(with parameters: [String: Any], boundary: String) -> Data {
        
        let lineBreak = "\r\n"
        var body = Data()
        
        for (key, value) in parameters {
            if let photo = value as? Media {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(key)\"; filename=\"\(photo.filename)\"\(lineBreak)")
                body.append("Content-Type: \(photo.mimeType + lineBreak + lineBreak)")
                body.append(photo.data)
                body.append(lineBreak)
            } else if let photos = value as? [Media] {
                for photo in photos {
                    let k = photo.key == "" ? key : photo.key
                    body.append("--\(boundary + lineBreak)")
                    body.append("Content-Disposition: form-data; name=\"\(k)\"; filename=\"\(photo.filename)\"\(lineBreak)")
                    body.append("Content-Type: \(photo.mimeType + lineBreak + lineBreak)")
                    body.append(photo.data)
                    body.append(lineBreak)
                }
            } else {
                body.append("--\(boundary + lineBreak)")
                body.append("Content-Disposition: form-data; name=\"\(key)\"\(lineBreak + lineBreak)")
                body.append("\(value)\(lineBreak)")
            }
        }
        body.append("--\(boundary)--\(lineBreak)")
        return body
    }
    var boundary: String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}

// MARK: - Extensions Data
extension Data {
    var json: Any? {
        let json = try? JSONSerialization.jsonObject(with: self, options: [])
        return json
    }
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            self.append(data)
        }
    }
}

extension Dictionary {
    var data: Data? {
        let data = try? JSONSerialization.data(withJSONObject: self, options: [])
        return data
    }
}





extension Data {
    
    /// Append string to Data
    ///
    /// Rather than littering my code with calls to `data(using: .utf8)` to convert `String` values to `Data`, this wraps it in a nice convenient little extension to Data. This defaults to converting using UTF-8.
    ///
    /// - parameter string:       The string to be added to the `Data`.
    
    mutating func append(_ string: String, using encoding: String.Encoding = .utf8) {
        if let data = string.data(using: encoding) {
            append(data)
        }
    }
}
