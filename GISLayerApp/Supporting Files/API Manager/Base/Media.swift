//
//  Media.swift
//  Carzly
//
//  Created by Zuhair Hussain on 11/06/2019.
//  Copyright © 2019 Zuhair Hussain. All rights reserved.
//

import UIKit

struct Media {
    let key: String
    let filename: String
    let data: Data
    let mimeType: String
    
    init?(withImage image: UIImage, key: String = "") {
        self.key = key
        self.mimeType = "image/jpeg"
        self.filename = "image\(arc4random()).jpg"
        
        guard let data = image.jpegData(compressionQuality: 0.3) else { return nil }
        self.data = data
    }
}
