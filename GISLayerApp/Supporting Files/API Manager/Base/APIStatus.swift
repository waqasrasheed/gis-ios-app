//
//  APIStatus.swift
//  Carzly
//
//  Created by Zuhair Hussain on 11/06/2019.
//  Copyright © 2019 Zuhair Hussain. All rights reserved.
//

import Foundation

private struct AssociatedKeys {
    static var message = "APIMessageKey"
    static var code = "APICodeKey"
    static var handle = "handle"
}


enum APIResponseStatus {
    case success, failure
}

struct APIStatus {
    
    var status = APIResponseStatus.failure
    
    static let success = APIStatus(status: .success)
    static let failure = APIStatus(status: .failure)
    
    
    init(status: APIResponseStatus) {
        self.status = status
    }
    
    var message: String = ""
    var code: Int = 0
    
    static func successWith(message: String, code: Int) -> APIStatus {
        var s = APIStatus.success
        s.message = message
        s.code = code
        return s
    }
    static func failureWith(message: String, code: Int) -> APIStatus {
        var s = APIStatus.failure
        s.message = message
        s.code = code
        return s
    }
    
    
    
    
    static var noNetwork: APIStatus {
        return APIStatus.failureWith(message: "Internet connection appears offline", code: 503)
    }
    static var unknown: APIStatus {
        return APIStatus.failureWith(message: "Unknown error", code: 404)
    }
    static var invalidRequest: APIStatus {
        return APIStatus.failureWith(message: "Invalid request", code: 422)
    }
    static var invalidResponse: APIStatus {
        return APIStatus.failureWith(message: "Invalid response", code: 422)
    }
}


func ==(left: APIStatus, right: APIStatus) -> Bool {
    return left.status == right.status
}
