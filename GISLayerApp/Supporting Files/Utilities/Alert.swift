//
//  Alert.swift
//  SSO Login
//
//  Created by Zuhair Hussain on 12/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import UIKit

class Alert {
    
    
    static func show(title: String?, message: String?, from controller: UIViewController) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    static func show(error message: String?, from controller: UIViewController) {
        let alert = UIAlertController(title: "Xeena", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        controller.present(alert, animated: true, completion: nil)
    }
    
}
