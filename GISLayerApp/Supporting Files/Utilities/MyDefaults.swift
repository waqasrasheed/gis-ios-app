//
//  MyDefaults.swift
//  SSO Login
//
//  Created by Zuhair Hussain on 12/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import SwiftyJSON


class MyDefaults {
    
    static let shared = MyDefaults()
    private var currentUser = UserModel()
    
    struct Keys {
        static let user = "kUser"
    }
    
    init() {
        if let object = UserDefaults.standard.object(forKey: Keys.user) as? [String: Any] {
            let json = JSON(object)
            currentUser = UserModel(json: json)
        }
    }
    
    
    func clear() {
        UserDefaults.standard.removeObject(forKey: Keys.user)
    }
    
    var user: UserModel {
        get {
            return currentUser
        }
        set {
            currentUser = newValue
            UserDefaults.standard.set(newValue.json, forKey: Keys.user)
        }
    }
    
    
}
