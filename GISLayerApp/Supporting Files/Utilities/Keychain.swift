//
//  Keychain.swift
//  SSO Login
//
//  Created by Zuhair Hussain on 12/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import Foundation
import KeychainSwift

class Keychain {
    
    // MARK: - Properties
    static let shared = Keychain()
    private init() {}
    private let keychain = KeychainSwift()

    struct Keys {
        static let accessToken = "kAccessToken"
        static let refreshToken = "kRefreshToken"
    }
    
    func clear() {
        let s = keychain.clear()
        print(s ? "Keychain cleared" : "Keychain clearing error")
    }
    
    var accessToken: String? {
        set {
            keychain.set(newValue ?? "", forKey: Keys.accessToken)
        }
        get {
            return keychain.get(Keys.accessToken)
            
        }
    }
    var refreshToken: String? {
        set {
            keychain.set(newValue ?? "", forKey: Keys.refreshToken)
        }
        get {
            return keychain.get(Keys.refreshToken)
            
        }
    }
    
}
