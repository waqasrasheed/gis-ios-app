//
//  UserModel.swift
//  SSO Login
//
//  Created by Zuhair Hussain on 12/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import SwiftyJSON

class UserModel {
    var id = ""
    var username = ""
    var fullName = ""
    var email = ""
    var gender = ""
    var phone = ""
    var photo = ""
    var dateOfBirth = ""
    var streetAddress = ""
    var activated = false
    var createdDate = ""
    var city = CityModel()
    var province = ProvinceModel()
    var country = CountryModel()
    var userRoles = [UserRoleModel]()
    
    init() {}
    init(json: JSON) {
        id = json["id"].stringValue
        username = json["username"].stringValue
        fullName = json["fullName"].stringValue
        email = json["email"].stringValue
        gender = json["gender"].stringValue
        phone = json["phone"].stringValue
        photo = json["photo"].stringValue
        dateOfBirth = json["dateOfBirth"].stringValue
        streetAddress = json["streetAddress"].stringValue
        activated = json["activated"].boolValue
        createdDate = json["createdDate"].stringValue
        
        city = CityModel(json: json["city"])
        province = ProvinceModel(json: json["province"])
        country = CountryModel(json: json["country"])
        
        let arr = json["userRoles"]["userRoles"].arrayValue
        for a in arr {
            userRoles.append(UserRoleModel(json: a))
        }
    }
    
    var json: [String: Any] {
        let j: [String: Any] = [
            "id": id,
            "username": username,
            "fullName": fullName,
            "email": email,
            "gender": gender,
            "phone": phone,
            "photo": photo,
            "dateOfBirth": dateOfBirth,
            "streetAddress": streetAddress,
            "activated": activated,
            "createdDate": createdDate,
            "city": city.json,
            "province": province.json,
            "country": country.json
        ]
        return j
    }
    
}

class CountryModel {
    var id = ""
    var name = ""
    var code = ""
    
    init() {}
    init(json: JSON) {
        id = json["id"].stringValue
        name = json["countryName"].stringValue
        code = json["countryCode"].stringValue
    }
    
    
    var json: [String: Any] {
        return ["id": id, "countryName": name, "countryCode": code]
    }
}
class ProvinceModel {
    var id = ""
    var name = ""
    
    init() {}
    init(json: JSON) {
        id = json["id"].stringValue
        name = json["provinceName"].stringValue
    }
    
    var json: [String: Any] {
        return ["id": id, "provinceName": name]
    }
}
class CityModel {
    var id = ""
    var name = ""
    
    init() {}
    init(json: JSON) {
        id = json["id"].stringValue
        name = json["cityName"].stringValue
    }
    
    var json: [String: Any] {
        return ["id": id, "cityName": name]
    }
}

class UserRoleModel {
    var id = ""
    var role = RoleAbbriviation.buyer
    
    init() {}
    init(json: JSON) {
        id = json["id"].stringValue
        role = RoleAbbriviation(rawValue: json["role"]["name"].stringValue) ?? .buyer
    }
}


enum RoleAbbriviation:String {
    case buyer = "BY"
    case shopKeeper = "SK"
    case courier = "CR"
    case promoter = "PR"
    case serviceProvider = "SP"
    case guest = "GTs"
}
