//
//  LandingController.swift
//  SSO Login
//
//  Created by Zuhair Hussain on 12/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import UIKit
import GISFramework

class LandingController: UIViewController {

    @IBOutlet weak var lblName: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        lblName.text = MyDefaults.shared.user.fullName
    }
    
    @IBAction func didTapLogoutButton(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: "Are you sure you want to logout?", message: nil, preferredStyle: .actionSheet)
        actionSheet.addAction(UIAlertAction(title: "Logout", style: .destructive, handler: { _ in
            
            MyDefaults.shared.clear()
            Keychain.shared.clear()
            let controller = LoginController(nibName: LoginController.name, bundle: nil)
            self.navigationController?.setViewControllers([controller], animated: true)
            
        }))
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        present(actionSheet, animated: true, completion: nil)
    }
    
    
    @IBAction func btnMapPressed(_ sender: UIButton) {
        
//        let bundle = Bundle(for: TRMapViewController.self)
//        let myVC = TRMapViewController(nibName: "TRMapViewController", bundle: bundle)
        
//        let myVC = TRMapViewController(baseURL: "https://map.demo-aws.meeraspace.com", accessToken: "eyJhbGciOiJSUzI1NiIsImtpZCI6IjQ0MjkyZGFkMjkyNjkwNzU4OTI5ZTZlYTE0ZmFhYTBhZTlhMTcxODIifQ.eyJpc3MiOiJodHRwczovL3Nzby5kZW1vLWF3cy5tZWVyYXNwYWNlLmNvbSIsInN1YiI6IkNpUmpOVFU0WlRrNU5DMDFNamd3TFRRNFpERXRPVEZrTVMxaU5UTXpNVFpqTWpNd1lXSVNCV3h2WTJGcyIsImF1ZCI6Im1lZXJhLWR4IiwiZXhwIjoxNTgxNzYzMjQxLCJpYXQiOjE1ODE2NzY4NDEsImF0X2hhc2giOiJnVjNJMk9FWHQ2aTVLd3Y3NDJVcHhnIiwiZW1haWwiOiJ0YXJnZXRtb2hzaW5AZ21haWwuY29tIiwiZW1haWxfdmVyaWZpZWQiOnRydWUsIm5hbWUiOiJtb2hzaW4ifQ.q2tidfXooTK_N3o8AxzbhsTVGInsrjOGqnoHA7zM8oTYw4-gzZhbT5N4LT_GGShfZ-nVT7s69CV6ZFIdopzRuxK3pcIXCdxRvpjZlLmWqoUC9QhmF_t_RLdOvKM72ZkBKVZWwVWtFNBc8_7ev-KX479dYK2RI35SHTMhAchSdBYN303oEOa0Me_gMRAAsZageASFTyYVYMuvNdsi3U9ypvAgE-sfBSormHBLdnAbVJ6jfyo7FE_acomqp1KtAOt33NYRhQPL43fcrfT_g38RNogDHcWFuvP8XI7iuQ1oFWQoXyuTWw0DOf81Or3Wxlvrmbwu9W3WIrzru09JGzN_aA" /*Keychain.shared.accessToken*/, refreshAccessToken: Keychain.shared.refreshToken, oauthswift: oauthswift) { (isSuccess, accessToken, refreshAccessToken) in
//
//        }
        
        
        let myVC = GISMapViewController(baseURL: "https://map.\(domainName)", oauthswift: oauthswift)
        
        self.navigationController?.pushViewController(myVC, animated: true)
    }
}
