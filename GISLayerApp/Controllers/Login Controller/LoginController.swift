//
//  LoginController.swift
//  SSO Login
//
//  Created by Zuhair Hussain on 12/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import UIKit
import TransitionButton
import SwiftValidator
import GISFramework

class LoginController: UIViewController {

    @IBOutlet weak var txtPhoneNumber: UITextField!
    @IBOutlet weak var txtPassword: UITextField!
    @IBOutlet weak var btnLogin: TransitionButton!
    @IBOutlet weak var imgIconEye: UIImageView!
    @IBOutlet weak var lblCountryCode: UILabel!
    @IBOutlet weak var lblErrorPhoneNumber: UILabel!
    @IBOutlet weak var lblErrorPassword: UILabel!
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
//        lblCountryCode.text = "+92"
//        txtPhoneNumber.text = "3350011220"
//        txtPassword.text = "password"
        
//        lblCountryCode.text = "+92"
//        txtPhoneNumber.text = "987987987"
//        txtPassword.text = "password"
    }
    
}

// MARK: - Actions
extension LoginController {
    
    @IBAction func didTapCountryCodeButton(_ sender: UIButton) {
        CountryPicker.shared.show(from: self) { (country) in
            self.lblCountryCode.text = country.phoneCode
        }
    }
    @IBAction func didTapShowHidePasswordButton(_ sender: UIButton) {
        if imgIconEye.tag == 0 {
            imgIconEye.tag = 1
            imgIconEye.image = "eye_unselected".bundleImage
            txtPassword.isSecureTextEntry = false
        } else {
            imgIconEye.tag = 0
            imgIconEye.image = "eye_selected".bundleImage
            txtPassword.isSecureTextEntry = true
        }
    }
    @IBAction func didTapLoginButton(_ sender: TransitionButton) {
        view.endEditing(true)
        validate { (status) in
            if status == .success {
                self.login()
            } else {
                sender.animate(with: .shake)
            }
        }
        
    }
}


// MARK: - Utility Methods
extension LoginController {
    func setupUI() {
        let currentCountry = CountryPicker.shared.currentCountry()
        lblCountryCode.text = currentCountry.phoneCode
        
        validator.registerField(txtPhoneNumber, errorLabel: lblErrorPhoneNumber, rules: [RequiredRule(message: "Provide phone number"), MinLengthRule(length: 6, message: "Provide valid phone number")])
        validator.registerField(txtPassword, errorLabel: lblErrorPassword, rules: [RequiredRule(message: "Enter password"), MinLengthRule(length: 8)])
    }
    
    func validate(_ completion: (_ status: Status) -> Void) {
        resetErrors()
        validator.validate { errors in
            
            if errors.count == 0 {
                completion(Status.success)
                return
            } else {
                for error in errors {
                    error.1.errorLabel?.text = error.1.errorMessage
                }
            }
            self.view.layoutIfNeeded()
            completion(.failure)
        }
    }
    func resetErrors() {
        [lblErrorPhoneNumber, lblErrorPassword].forEach { $0?.text = "" }
    }
}


// MARK: - Web APIs
extension LoginController {
    func login() {
        view.isUserInteractionEnabled = false
        btnLogin.startAnimation()
        
        APIManager.auth.login("\(lblCountryCode.text!) \(txtPhoneNumber.text!)" , password: txtPassword.text!) { (user, error) in
            self.view.isUserInteractionEnabled = true
            self.btnLogin.stopAnimation(revertAfterDelay: 0)
            
            if error == nil {
                MyDefaults.shared.user = user
                
                //let  controller = LandingController(nibName: LandingController.name, bundle: nil)
                let controller = GISMainTabController()
                self.navigationController?.setViewControllers([controller], animated: true)
                
            } else {
                Alert.show(error: error!.errorDescp ?? "Unexpected error", from: self)
            }
            
        }
    }
}
