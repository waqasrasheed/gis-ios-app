//
//  CreateMapModel.swift
//  GISFramework
//
//  Created by Muhammad Arslan Khalid on 02/03/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit

struct CreateMapModel {

    var name:String?
    var crs:String? = "3857"
    var descp:String?
    var minZoom:String? = "2"
    var maxZoom:String? = "22"
    var minLon:String?
    var maxLon:String?
    var minLat:String?
    var maxLat:String?
    var isMapPrivate = true
}

