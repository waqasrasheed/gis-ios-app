//
//  GISServerModel.swift
//  GISFramework
//
//  Created by Muhammad Arslan Khalid on 17/02/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit
import SwiftyJSON

class GISServerModel: NSObject {

    var featureServerURL: String?
    var changedDate: String?
    var createdDate: String?
    var createdBy, proxyURL, changedBy: String?
    var name, datumDescription: String?
    var attributes: String?
    var disabled: Bool?
    var id: Int?
    var url: String?
    
    init(json:JSON) {
        self.featureServerURL = json["featureServerURL"].string
        self.changedDate = json["changedDate"].string
        self.createdDate = json["createdDate"].string
        self.createdBy = json["createdBy"].string
        self.proxyURL = json["proxyURL"].string
        self.changedBy = json["changedBy"].string
        self.name = json["name"].string
        self.datumDescription = json["datumDescription"].string
        self.attributes = json["attributes"].string
        self.disabled = json["disabled"].bool
        self.id = json["id"].int
        self.url = json["url"].string
    }
    
    
}

class GISPropertyModel {
    
    var id: String!
    
    var FID: String = ""
    var UWI: String = ""
    var DISPLAY_CLASS: String = ""
    var LABEL: String = ""
    var WELL_CLASS: String = ""
    
    init(json: JSON) {
        
        self.id = json["id"].string
        self.FID = json["FID"].stringValue
        self.UWI = json["UWI"].stringValue
        self.DISPLAY_CLASS = json["DISPLAY_CLASS"].stringValue
        self.LABEL = json["LABEL"].stringValue
        self.WELL_CLASS = json["WELL_CLASS"].stringValue
    }
    
}

class StaticMapModel {
    
    var image: String = ""
    var name: String = ""
    var url: String = ""
    
    init(image: String, name: String, url: String) {
        
        self.image = image
        self.name = name
        self.url = url
    }
    
    static func getStaticMapData() -> [StaticMapModel] {
        
        var arrModel = [StaticMapModel]()
        
        arrModel.append(StaticMapModel(image: "Buildings", name: "Buildings",url: "/infrastructure/buildings"))
        arrModel.append(StaticMapModel(image: "Schools", name: "Schools",url: "/infrastructure/schools"))
        arrModel.append(StaticMapModel(image: "Hospitals", name: "Hospitals",url: "/infrastructure/hospitals"))
        arrModel.append(StaticMapModel(image: "Pipelines", name: "Pipelines",url: "/infrastructure/pipelines"))
        arrModel.append(StaticMapModel(image: "Intrastructure", name: "Intrastructure",url: "/infrastructure/all"))
        
        return arrModel
    }
}

class GISProfileModel {
    
    var userID: String!
    var fullName: String!
    var photo: String!
    
    init(json: JSON) {
        
        self.userID =  json["userID"].string
        self.photo =  json["photo"]["aPIID"].string
        self.fullName =  json["fullName"].string
    }
}
