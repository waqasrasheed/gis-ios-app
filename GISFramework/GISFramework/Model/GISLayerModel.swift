//
//  GISLayerModel.swift
//  GISFramework
//
//  Created by Muhammad Arslan Khalid on 14/02/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit
import SwiftyJSON

class GISLayerModel: Equatable {
    
    var controls: String?
    var maxZoom: Int?
    var maxY, maxX: Double?
    var descp: String?
    var units: String?
    var userSubject: String?
    var changedDate: String?
    var srs: String?
    var changedBy: String?
    var id: Int?
    var height: Double?
    var minZoom: Int?
    var label: String?
    var url, maxResolution: String?
    var isDefault: Bool?
    var createdDate: String?
    var minY: Double?
    var minX: Double?
    var createdBy: String?
    var scales:String?
    var width: Double?
    var name: String?
    var mapImage: String = ""
    var parameters: String?
    var layers: [GISLayer]?
    var baseLayers: [GISBaseLayer]?
    
    var isActive: Bool = false
    
    var isBaseLayerExpended = false
    
    //    override init() {
    //        super.init()
    //    }
    
    init(json:JSON) {
        self.controls = json["controls"].string
        self.maxZoom = json["maxZoom"].int
        self.maxY = json["maxY"].double
        self.maxX = json["maxX"].double
        self.descp = json["description"].string
        self.units = json["units"].string
        self.userSubject = json["userSubject"].string
        self.changedDate = json["changedDate"].string
        self.srs = json["srs"].string
        self.changedBy = json["changedBy"].string
        if let arr = json["layers"].array { self.layers = arr.map { (data) -> GISLayer in
            GISLayer(json: data)
            }}
        self.id = json["id"].int
        self.height = json["height"].double
        self.minZoom = json["minZoom"].int
        self.label = json["label"].string
        self.url = json["url"].string
        self.maxResolution = json["maxResolution"].string
        self.isDefault = json["isDefault"].bool
        self.createdDate = json["createdDate"].string
        self.minY = json["minY"].double
        if let arr = json["baseLayers"].array {self.baseLayers = arr.map({ (data) -> GISBaseLayer in
            GISBaseLayer(json: data)
        })}
        self.minX = json["minX"].double
        self.createdBy = json["createdBy"].string
        self.scales = json["scales"].string
        self.width = json["width"].double
        self.name = json["name"].string
        self.parameters = json["parameters"].string
        self.mapImage = self.getMapImageWithMap(name: self.name)
    }
    
    
    static func == (lhs: GISLayerModel, rhs: GISLayerModel) -> Bool {
        return lhs.id == rhs.id
    }
    
    static func != (lhs: GISLayerModel, rhs: GISLayerModel) -> Bool {
        return lhs.id != rhs.id
    }
}

class GISLayer: NSObject {
    
    var defaultActive: Bool?
    var displayOrder: Int?
    var layerDescription: String?
    var useSingleTile: Bool?
    var userSubject: String?
    var legendURL: String?
    var catalogAttribute: String?
    var changedDate: String?
    var featureAttribute: String?
    var changedBy: String?
    var featureType, geomName: String?
    var id: Int?
    var layerType: String?
    var label: String?
    var geometryExpr, catalogName: String?
    var queryable: Bool?
    var pollInterval: String?
    var isDefault: Bool?
    var createdDate: String?
    var rendered: Bool = false
    var createdBy: String?
    var name: String?
    var style: String?
    var featureCatalog: FeatureCatalog?
    var attributes: [GISAttribute]?
    var cqlFilter, layerOpacity, featureTypesWithLegendGraphics, wfsFilter: String?
    var mapServer: GISMapServer?
    var bounds: GISBoundsModel?
    var map: GISLayerModel?
    
    
    var isTopLayerExpended = false
    var isSelected = false {
        didSet {
            print("isSelectChange \(self.isSelected)")
        }
    }
    var sliderOpacityValue: Float = 1
    
    init(json:JSON) {
        
        self.defaultActive = json["defaultActive"].bool
        self.displayOrder = json["displayOrder"].int
        self.layerDescription = json["layerDescription"].string
        self.useSingleTile = json["useSingleTile"].bool
        self.userSubject = json["userSubject"].string
        self.legendURL = json["legendUrl"].string
        self.catalogAttribute = json["catalogAttribute"].string
        self.changedDate = json["changedDate"].string
        self.featureAttribute = json["featureAttribute"].string
        self.changedBy = json["changedBy"].string
        self.featureType = json["featureType"].string
        self.geomName = json["geomName"].string
        self.id = json["id"].int
        self.map = GISLayerModel(json: json["map"])
        self.layerType = json["layerType"].string
        self.mapServer =  GISMapServer(json: json["mapServer"])
        self.label = json["label"].string
        self.geometryExpr = json["geometryExpr"].string
        self.catalogName = json["catalogName"].string
        self.queryable = json["queryable"].bool
        self.pollInterval = json["pollInterval"].string
        self.isDefault = json["isDefault"].bool
        self.createdDate = json["createdDate"].string
        self.rendered = json["rendered"].boolValue
        self.createdBy = json["createdBy"].string
        self.bounds = GISBoundsModel(json: json["bounds"])
        self.name = json["name"].string
        self.style = json["style"].string
        self.featureCatalog = FeatureCatalog(json: json["featureCatalog"])
        if let arr = json["attributes"].array{self.attributes = arr.map({ (data) -> GISAttribute in
            GISAttribute(json: data)
        })}
        self.cqlFilter = json["cqlFilter"].string
        self.layerOpacity = json["layerOpacity"].string
        self.featureTypesWithLegendGraphics = json["featureTypesWithLegendGraphics"].string
        self.wfsFilter = json["wfsFilter"].string
        
        isSelected = self.rendered
    }
    
    static func == (lhs: GISLayer, rhs: GISLayer) -> Bool {
        return lhs.id == rhs.id
    }
    
    static func != (lhs: GISLayer, rhs: GISLayer) -> Bool {
        return lhs.id != rhs.id
    }
    
    func validate() -> Bool {
        
        if mapServer == nil {
            return false
        }
        if !AppUtility.hasValidText(mapServer!.url) {
            return false
        }
        if !AppUtility.hasValidText(mapServer!.featureServerURL) {
            return false
        }
        
        if !AppUtility.hasValidText(name) {
            return false
        }
        
        if id == nil {
            return false
        }
        
        return true
    }
}





class GISAttribute {
    var isGeometry, nullable: Bool?
    var displayName: String?
    var attributeDescription: String?
    var type: String?
    var maxOccures: Int?
    var changedDate: String?
    var createdDate:String?
    var createdBy: String?
    var minOccures: Int?
    var changedBy: String?
    var name: String?
    var id: Int?
    var value: String?
    init(json:JSON) {
        self.isGeometry = json["isGeometry"].bool
        self.nullable = json["nullable"].bool
        self.displayName = json["displayName"].string
        self.attributeDescription = json["attributeDescription"].string
        self.type = json["type"].string
        self.maxOccures = json["maxOccures"].int
        self.changedDate = json["changedDate"].string
        self.createdDate = json["createdDate"].string
        self.minOccures = json["minOccures"].int
        self.changedBy = json["changedBy"].string
        self.name = json["name"].string
        self.id = json["id"].int
        self.value = json["value"].string
    }
}

class GISMapServer {
    
    var featureServerURL: String?
    var changedDate: String?
    var createdDate: String?
    var createdBy: String?
    var proxyURL: String?
    var changedBy: String?
    var name, mapServerDescription: String?
    var attributes: String?
    var disabled: Bool?
    var id: Int?
    var url: String?
    init(json:JSON) {
        
        self.featureServerURL = json["featureServerUrl"].string
        self.changedDate = json["changedDate"].string
        self.createdDate = json["createdDate"].string
        self.createdBy = json["createdBy"].string
        self.proxyURL = json["proxyURL"].string
        self.changedBy = json["changedBy"].string
        self.name = json["name"].string
        self.mapServerDescription = json["mapServerDescription"].string
        self.disabled = json["disabled"].bool
        self.id = json["id"].int
        self.url = json["url"].string
        
    }
    
}

class GISBoundsModel
{
    var minY:Double!
    var minX:Double!
    var SRS:String!
    var maxY:Double!
    var maxX:Double!
    
    init(json:JSON) {
        self.minX = json["minX"].double
        self.minY = json["minY"].double
        self.SRS = json["SRS"].string
        self.maxY = json["maxY"].double
        self.maxX = json["maxX"].double
    }
}


class GISBaseLayer {
    var layerType: LayerType?
    var apiKey: String?
    var displayOrder: Int?
    var baseLayerDescription, label: String?
    var url: String?
    var changedDate: String?
    var sortKey: String?
    var createdDate: String?
    var rendered: Bool?
    var createdBy: String?
    var changedBy: String?
    var featureType: String?
    var name: String?
    var id: Int?
    var maps: [GISLayerModel]?
    
    var isSelected = false
    
    init(json:JSON) {
        
        //self.layerType = json["layerType"].string
        self.layerType = LayerType(rawValue: json["layerType"].string ?? "")
        
        if let arr = json["maps"].array {self.maps = arr.map({ (data) -> GISLayerModel in
            GISLayerModel(json: data)
        })}
        self.apiKey = json["apiKey"].string
        self.displayOrder = json["displayOrder"].int
        self.baseLayerDescription = json["baseLayerDescription"].string
        self.label = json["label"].string
        self.url = json["url"].string
        self.changedDate = json["changedDate"].string
        self.sortKey = json["sortKey"].string
        self.createdDate = json["createdDate"].string
        self.rendered = json["rendered"].bool
        self.createdBy = json["createdBy"].string
        self.changedBy = json["changedBy"].string
        self.featureType = json["featureType"].string
        self.name = json["name"].string
        self.id = json["id"].int
        
        isSelected = self.rendered ?? false
    }
    
    func validate() -> Bool {
        
        if !AppUtility.hasValidText(url) {
            return false
        }
        if !AppUtility.hasValidText(name) {
            return false
        }
        
        return true
    }
}

class FeatureCatalog {
    
    var name: String = ""
    var displayOrder: Int?
    init(json:JSON) {
        
        self.name = json["name"].stringValue
        self.displayOrder = json["displayOrder"].int
    }
    
}

extension Array where Element: GISBaseLayer {
    var isAllSelected: Bool {
        set {
            for m in self {
                m.isSelected = newValue
            }
        }
        get {
            let f = self.filter { $0.isSelected == false }
            return f.count == 0
        }
        
    }
}

extension GISLayerModel {
    func selectedLayers() -> [GISLayer] {
        let filtered = layers?.filter({ $0.isSelected })
        return filtered ?? []
    }
    func selectedBaseLayers() -> [GISBaseLayer] {
        let filtered = baseLayers?.filter({ $0.isSelected })
        return filtered ?? []
    }
    func getUnSelectedLayers() -> [GISLayer] {
        let filtered = layers?.filter({ $0.isSelected == false })
        return filtered ?? []
    }
    func getUnSelectedBaseLayers() -> [GISBaseLayer] {
        let filtered = baseLayers?.filter({ $0.isSelected == false })
        return filtered ?? []
    }
    
    func unSelectedLayers() {
        
        //let _ = layers?.map({$0.isSelected = false})
        // Only unselect those layer which selected by user
        _ = layers?.map({ (model) -> GISLayer in
            
            if model.rendered == false {
                
                model.isSelected = false
            }
            else {
                model.isSelected = true
            }
            
            return model
        });
        
        //_ = layers?.map({$0.rendered == false ? $0.isSelected = false : $0.isSelected = true })
        
    }
    func unSelectedBaseLayers() {
        _ = baseLayers?.map({$0.isSelected = false})
    }
}

extension GISBaseLayer {
    
    enum LayerType: String {
        case ESRI = "ESRI"
        case TileXYZ = "TileXYZ"
        case none = "none"
    }
}

extension GISLayerModel {
    
    func getMapImageWithMap(name: String?) -> String {
        
        let loweredCaseName = name?.lowercased()
        
        if loweredCaseName == "oil and gas" {
            return "map_2.png"
        }
        else if loweredCaseName == "ncsi map" {
            return "map_3.png"
        }
        else if loweredCaseName == "charts" {
            return "map_4.png"
        }
        else if loweredCaseName == "consumer status" {
            return "map_1.png"
        }
        else if loweredCaseName == "Map" {
            return "map_1.png"
        }
        else if loweredCaseName == "mymapnewone" {
            return "map_5.png"
        }
        
        return "default_map.png"
    }
}

enum DrawPlotPointsEnum
{
    case  singlePoint,line,polygone
}


