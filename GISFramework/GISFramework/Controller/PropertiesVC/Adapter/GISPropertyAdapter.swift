//
//  GISPropertyAdapter.swift
//  GISFramework
//
//  Created by Waqas Rasheed on 03/03/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit

class GISPropertyAdapter: NSObject{
    
    weak var tableView: UITableView!
    var bundle: Bundle!
    var propertyModel: [GISPropertyModel] = [] {
        didSet {
            self.tableView.reloadData()
        }
    }
    
    var parent: GISPropertiesVC? {
        return self.tableView.viewContainingController() as? GISPropertiesVC
    }
    
    init(_ tableView: UITableView) {
        super.init()
        
        self.tableView = tableView
        configure()
    }
    
    func configure() {
        
        bundle = Bundle(for: type(of: self))
        
        tableView.register(UINib(nibName: "GISPropteryHeader", bundle: bundle), forHeaderFooterViewReuseIdentifier: "GISPropteryHeader")
        tableView.register(UINib(nibName: "GISPropterCell", bundle: bundle), forCellReuseIdentifier: "GISPropterCell")
        
        tableView.dataSource = self
        tableView.delegate = self
        
        self.tableView.rowHeight = UITableView.automaticDimension
        self.tableView.estimatedRowHeight = 36
        
        self.tableView.sectionHeaderHeight = UITableView.automaticDimension
        self.tableView.estimatedSectionHeaderHeight = 44
        
    }
}


extension GISPropertyAdapter: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 36
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "GISPropteryHeader") as! GISPropteryHeader
        
        return view
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return propertyModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "GISPropterCell", for: indexPath) as! GISPropterCell
        
        let model = propertyModel[indexPath.row]
        
        cell.lblFirst.text = model.UWI
        cell.lblSecond.text = model.DISPLAY_CLASS
        cell.lblThird.text = model.LABEL
        
        return cell
    }
    
    
    
}


