//
//  GISPropertiesVC.swift
//  GISFramework
//
//  Created by Waqas Rasheed on 03/03/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit

class GISPropertiesVC: UIViewController {
    
    @IBOutlet weak var tblProperties: UITableView!
    var adapter: GISPropertyAdapter!
    
    private var propertyModel: [GISPropertyModel] = []
    
    init(properties: [GISPropertyModel]) {
        
        let bundle = Bundle(for: type(of: self))
        super.init(nibName: "GISPropertiesVC", bundle: bundle)
        
        self.modalPresentationStyle = .overFullScreen
        self.modalTransitionStyle = .crossDissolve
        
        self.propertyModel = properties
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        adapter = GISPropertyAdapter(tblProperties)
        adapter.propertyModel = self.propertyModel
    }
    
    @IBAction func btnbackPressed(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    deinit {
        print("[SideMenuController] deinit")
    }
}
