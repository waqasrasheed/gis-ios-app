//
//  GISWKWebViewController.swift
//  GISFramework
//
//  Created by Waqas Rasheed on 03/03/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit
import WebKit

class GISWKWebViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet weak var topBar: GISTopBar!
    @IBOutlet weak var navHeight: NSLayoutConstraint!
    var isHidderTopBar: Bool = false;
    
    @IBOutlet weak var webView: WKWebView!
    
    var urlString: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setTopBarGUI()
        webView.navigationDelegate = self
        
        //let config = WKWebViewConfiguration()
        //config.userContentController.add(self, name: "setToken")
        //self.webView = WKWebView(frame: self.view.bounds, configuration: config)
        
        //self.urlString = urlString + "?access_token=\(Network.accessToken!)"
        
        if let url = URL(string: urlString) {
            
            var request = URLRequest(url: url)
            
            request.addValue("Bearer \(Network.accessToken!)", forHTTPHeaderField: "Authorization")
            request.addValue("text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9", forHTTPHeaderField: "accept")
            request.addValue("GET", forHTTPHeaderField: "method")
            request.addValue("/infrastructure/buildings", forHTTPHeaderField: "path")
            //request.addValue("", forHTTPHeaderField: "accept")
            
            //request.addValue("__cfduid=d213dabbba0479037b9dfdb238ed89bf61572592603; organizationId=1; _ga=GA1.2.56317168.1582871353; __token_domain=.demo-aws.meeraspace.com; __Secure-id_token=\(Network.accessToken!); _gid=GA1.2.693498522.1583305684", forHTTPHeaderField: "cookie")
            
            
            webView.allowsBackForwardNavigationGestures = true
            webView.load(request)
        }
        
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        
        //print(navigationAction.request.allHTTPHeaderFields)
        
        //let accessToken = "Bearer \(Network.accessToken!)"
        
        let request = navigationAction.request
        
        if !(request.allHTTPHeaderFields?.keys.contains("Authorization") ?? true)  {
            
            //request.setValue(accessToken, forHTTPHeaderField: "Authorization")
            
            //decisionHandler(.cancel)
            
            //webView.load(request)
            decisionHandler(.allow)
        }
        else {
            decisionHandler(.allow)
        }
    }
    
    func setTopBarGUI()
    {
        if(isHidderTopBar == true)
        {
            navHeight.constant = 0;
            topBar.isHidden = true;
            return;
        }
        
        self.topBar.lblNavHeading.text = "Oman Map"
        
        self.topBar.btnBack.addTarget(self, action: #selector(self.btnBackPressed(_:)), for: UIControl.Event.touchUpInside)
        AppUtility.setNavHeightForIPhoneX(navHightRef: self.navHeight)
        
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        
        if presentingViewController == nil {
            self.navigationController?.popViewController(animated: true);
        }
        else {
            self.dismiss(animated: true, completion: nil);
        }
    }
}
