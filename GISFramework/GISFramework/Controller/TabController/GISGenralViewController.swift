//
//  GISGenralViewController.swift
//  GISFramework
//
//  Created by Waqas Rasheed on 02/03/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit

public class GISGenralViewController: UITabBarController {
    
    var maplist: GISMainTabController!
    var dataSet: GISMainTabController!
    var server: GISMainTabController!
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.loadMapTabbar()
        
    }
    
    
    deinit {
        print("General tabbar deinit")
    }
    
    
    
    func loadMapTabbar()
    {
        //UIColor.colorWithRGB(255, 76, 32, opacity: 1.0);
        let mapController = GISMapViewController()
        maplist = GISMainTabController()
        //mainTabController.loadMaplist()
        let createMap = GISCreateMapVC()
        
        dataSet = GISMainTabController()
        //dataSet.loadDataset()
        
        server = GISMainTabController()
        //server.loadServer()
        //mapCtrl.layerData = model
        self.setViewControllers([mapController,maplist,createMap,dataSet,server], animated: true);
        
        guard let _ = self.tabBar.items else {
            return;
        }
        
        let font = UIFont(name: "Roboto-Medium", size: 11);
        UITabBarItem.appearance().setTitleTextAttributes([kCTFontAttributeName as NSAttributedString.Key: font!], for: .normal)
        
        //self.tabBar.barTintColor = UIColor.white;
        //self.tabBar.tintColor = AppCustomColor.PrimaryDarkColor
        let bundle = Bundle(for: GISMapViewController.self)
        
        
        
        let tabItem1 = self.tabBar.items![0];
        tabItem1.title = "Oman Map";
        tabItem1.selectedImage = UIImage(named: "home_enable", in: bundle, compatibleWith: nil)
        tabItem1.image = UIImage(named: "home_disabled", in: bundle, compatibleWith: nil)
        
        let tabItem2 = self.tabBar.items![1];
        tabItem2.title = "Map List"
        tabItem2.selectedImage = UIImage(named: "map_enable", in: bundle, compatibleWith: nil)
        tabItem2.image = UIImage(named: "map_disabled", in: bundle, compatibleWith: nil)
        
        let tabItem3 = self.tabBar.items![2];
        tabItem3.title = "Add Map";
        tabItem3.selectedImage = UIImage(named: "add_enable 2", in: bundle, compatibleWith: nil)
        tabItem3.image = UIImage(named: "add_disabled", in: bundle, compatibleWith: nil)
        
        let tabItem4 = self.tabBar.items![3];
        tabItem4.title = "Datasets";
        tabItem4.selectedImage = UIImage(named: "datacenter_enable", in: bundle, compatibleWith: nil)
        tabItem4.image = UIImage(named: "datacenter_disabled", in: bundle, compatibleWith: nil)
        
        let tabItem5 = self.tabBar.items![4];
        tabItem5.title = "Servers";
        tabItem5.selectedImage = UIImage(named: "server_enable", in: bundle, compatibleWith: nil)
        tabItem5.image = UIImage(named: "server_disabled", in: bundle, compatibleWith: nil)
        
        
    }
    
    private func navConttroller(controller:UIViewController) -> UINavigationController
    {
        let navController = UINavigationController(rootViewController: controller)
        navController.isNavigationBarHidden = true
        return navController
    }
    
    override public func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        
        if let title = item.title
        {
            if title == "Map List" {
                
                maplist.loadMaplist()
            }
            else if title == "Datasets" {
                
                dataSet.loadDataset()
            }
            else if title == "Servers" {
                server.loadServer()
            }
            
            
        }
        
        
    }
    
    
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    }
}

