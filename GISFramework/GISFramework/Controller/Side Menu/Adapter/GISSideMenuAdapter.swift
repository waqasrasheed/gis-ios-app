//
//  GISSideMenuAdapter.swift
//  GISFramework
//
//  Created by Zuhair Hussain on 13/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import UIKit
import SDWebImage

class GISSideMenuAdapter: NSObject {
    
    weak var tableView: UITableView!
    var didChange: ((_ valueChange: Float?, _ didSelect:Bool,_ selectedLayerGeoJSON: GISLayer?) -> Void)?
    var trwslayerData: GISLayerModel! //= HelperTRWMSLayer.shared.trwslayerData
    var selectedGISLayer: GISLayer?
    
    var bundle = Bundle()
    
    // MARK - Initializers
    init(_ tableView: UITableView, didChange: ((_ valueChange: Float?, _ didSelect:Bool,_ selectedLayerGeoJSON: GISLayer?) -> Void)?) {
        super.init()
        
        self.tableView = tableView
        self.didChange = didChange
        configure()
    }
    
    func configure() {
        
        bundle = Bundle(for: type(of: self))
        
        tableView.register(UINib(nibName: "GISSideMenuOptionHeader", bundle: bundle), forHeaderFooterViewReuseIdentifier: "GISSideMenuOptionHeader")
        tableView.register(UINib(nibName: "GISSideMenuOptionCell", bundle: bundle), forCellReuseIdentifier: "GISSideMenuOptionCell")
        tableView.register(UINib(nibName: "GISSliderCell", bundle: bundle), forCellReuseIdentifier: "GISSliderCell")
        
        tableView.dataSource = self
        tableView.delegate = self
        
    }
    
}


// MARK - Actions
extension GISSideMenuAdapter {
    @objc func didTapHeaderButton(_ sender: UIButton) {
        
        if sender.tag >= (trwslayerData.layers?.count ?? 0) {
            let isExpended = trwslayerData.isBaseLayerExpended
            trwslayerData.isBaseLayerExpended = !isExpended
            
        } else if sender.tag >= 0 {
            let isTopLayerExpended = trwslayerData.layers?[sender.tag].isTopLayerExpended ?? false
            trwslayerData.layers?[sender.tag].isTopLayerExpended = !isTopLayerExpended
            
            didChange?(nil,true,selectedGISLayer)
        }
        tableView.reloadSections([sender.tag], with: .fade)
        
    }
    @objc func didTapHeaderCheckMarkButton(_ sender: UIButton) {
        
        
        if sender.tag >= (trwslayerData.layers?.count ?? 0) {
            let isSelected = trwslayerData.baseLayers?.isAllSelected ?? false
            trwslayerData.baseLayers?.isAllSelected = !isSelected
            
            
            
        } else if sender.tag >= 0 {

            let model = trwslayerData.layers![sender.tag]
            
            let isSelected = model.isSelected
            model.isSelected = !isSelected
            
            if let layer = selectedGISLayer, layer == model {
               // view.btnRadio.isSelected = true
                self.selectedGISLayer = nil 
                self.tableView.reloadData()
            }
            
        }
        
        didChange?(nil,true,selectedGISLayer)

        
        
        
        tableView.reloadData()
    }
    
    @objc func didTapHeaderRadioButton(_ sender: UIButton) {
        
        let model = trwslayerData.layers![sender.tag]
        
        if model.isSelected {
            
            sender.isSelected = true
            selectedGISLayer = model
            print("selected")
            
            tableView.reloadData()
            didChange?(nil,false,selectedGISLayer)
        }
        else {
            print("Note selected")
        }
    }
    
    
}


// MARK - TableView DataSource and Delegate
extension GISSideMenuAdapter: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in tableView: UITableView) -> Int {
        if (trwslayerData.baseLayers?.count ?? 0) == 0 {
            return (trwslayerData.layers?.count ?? 0)
        }
        return (trwslayerData.layers?.count ?? 0) + 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section >= (trwslayerData.layers?.count ?? 0) {
            let isExpended = trwslayerData.isBaseLayerExpended
            return isExpended ? (trwslayerData.baseLayers?.count ?? 0) : 0
        }
        else {
            
            if section < trwslayerData.layers?.count ?? 0 {
                
                let isExpended = trwslayerData.layers![section].isTopLayerExpended
                return isExpended ? 1 : 0
            }
        }
        
        return 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier: "GISSideMenuOptionHeader") as! GISSideMenuOptionHeader
        view.btnRadio.isHidden = true
        
        if section >= (trwslayerData.layers?.count ?? 0) {
            
            let isAllSelected = trwslayerData.baseLayers?.isAllSelected ?? false
            view.lblTitle.text = "Base Maps"
            view.imgArrow.isHidden = false
            view.imgIconCheckMark.image = isAllSelected ? UIImage(named: "gBigcheck_mark_selected", in: bundle, compatibleWith: nil) : UIImage(named: "gBigcheck_mark_unselected", in: bundle, compatibleWith: nil)
            
        } else {
            
            let model = trwslayerData.layers![section]
            
            let isAllSelected = model.isSelected // ?? false
            view.lblTitle.text = model.label
            //view.imgArrow.isHidden = true
            view.imgIconCheckMark.image = isAllSelected ? UIImage(named: "gBigcheck_mark_selected", in: bundle, compatibleWith: nil) : UIImage(named: "gBigcheck_mark_unselected", in: bundle, compatibleWith: nil)
            
            view.btnRadio.isSelected = false
            view.btnRadio.isHidden = false
            if let layer = selectedGISLayer, layer == model {
                view.btnRadio.isSelected = true
            }
            
        }
        
        view.btnHeader.addTarget(self, action: #selector(didTapHeaderButton(_:)), for: .touchUpInside)
        view.btnCheckMark.addTarget(self, action: #selector(didTapHeaderCheckMarkButton(_:)), for: .touchUpInside)
        view.btnRadio.addTarget(self, action: #selector(didTapHeaderRadioButton(_:)), for: .touchUpInside)
        view.btnHeader.tag = section
        view.btnCheckMark.tag = section
        view.btnRadio.tag = section
        
        
        return view
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 30
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        
        if indexPath.section < (trwslayerData.layers?.count ?? 0) {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "GISSliderCell", for: indexPath) as! GISSliderCell
            
            let model = trwslayerData.layers?[indexPath.section]
            if let legendURL = model!.legendURL?.url {
                
                cell.legenIcon.sd_setImage(with: legendURL, placeholderImage: UIImage(named: "wmsHolder"), options: SDWebImageOptions.continueInBackground) { (img, error, cache, url) in
                    
                }
            }
            cell.opacitySlider.value = model!.sliderOpacityValue
            print(model!.sliderOpacityValue)
            cell.opacitySlider.tag = indexPath.section
            cell.opacitySlider.addTarget(self, action: #selector(sliderOpacityValucChange(_:)), for: .valueChanged)
            return cell
        }
        else {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "GISSideMenuOptionCell", for: indexPath) as! GISSideMenuOptionCell
            
            cell.lblTitle.text = trwslayerData.baseLayers?[indexPath.row].name
            
            let isAllSelected = trwslayerData.baseLayers?.isAllSelected ?? false
            if isAllSelected {
                //cell.imgIconCheckMark.image = UIImage(named: "check", in: bundle, compatibleWith: nil)
                cell.imgIconCheckMark.image = UIImage(named: "gBigcheck_mark_selected", in: bundle, compatibleWith: nil)
            } else {
                //cell.imgIconCheckMark.image = trwslayerData.baseLayers?[indexPath.row].isSelected == true ? UIImage(named: "check", in: bundle, compatibleWith: nil) : UIImage(named: "uncheck", in: bundle, compatibleWith: nil)
                cell.imgIconCheckMark.image = trwslayerData.baseLayers?[indexPath.row].isSelected == true ? UIImage(named: "gBigcheck_mark_selected", in: bundle, compatibleWith: nil) : UIImage(named: "gBigcheck_mark_unselected", in: bundle, compatibleWith: nil)
            }
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let isSelected = trwslayerData.baseLayers?[indexPath.row].isSelected ?? false
        trwslayerData.baseLayers?[indexPath.row].isSelected = !isSelected
        
        didChange?(nil,true,selectedGISLayer)
        
        tableView.reloadData()
    }
    
    @objc func sliderOpacityValucChange(_ sender: UISlider) {
        
        let model = trwslayerData.layers?[sender.tag]
        
        let currentValue = sender.value
        model?.sliderOpacityValue = currentValue
        
        print(currentValue)
        
        let isSelected = model!.isSelected
        didChange?(currentValue,isSelected,selectedGISLayer)
    }
}



class HeaderModel {
    var title = ""
    var isSelected = false
    var isExpended = false
    var options = [OptionModel]()
    
    init() {}
    init(title: String, options: [String]) {
        self.title = title
        
        for o in options {
            self.options.append(OptionModel(title: o))
        }
    }
    
}


class OptionModel {
    var title = ""
    var isSelected = false
    
    init(title: String) {
        self.title = title
    }
}
