//
//  GISSideMenuOptionHeader.swift
//  GISFramework
//
//  Created by Zuhair Hussain on 13/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import UIKit

class GISSideMenuOptionHeader: UITableViewHeaderFooterView {
    
    @IBOutlet weak var imgIconCheckMark: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imgArrow: UIImageView!
    @IBOutlet weak var btnCheckMark: UIButton!
    @IBOutlet weak var btnRadio: UIButton!
    @IBOutlet weak var btnHeader: UIButton!
    
    
}
