//
//  GISSliderCell.swift
//  GISFramework
//
//  Created by Waqas Rasheed on 24/02/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit

class GISSliderCell: UITableViewCell {

    @IBOutlet weak var legenIcon: UIImageView!
    @IBOutlet weak var opacitySlider: UISlider!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
