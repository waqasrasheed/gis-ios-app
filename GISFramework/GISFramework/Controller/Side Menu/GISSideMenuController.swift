//
//  GISSideMenuController.swift
//  GISFramework
//
//  Created by Zuhair Hussain on 13/02/2020.
//  Copyright © 2020 Zuhair Hussain. All rights reserved.
//

import UIKit

class GISSideMenuController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var cstTableViewLeading: NSLayoutConstraint!
    
    var trwslayerData: GISLayerModel!
    var selectedGISLayer: GISLayer?
    var adapter: GISSideMenuAdapter!
    
    var didChange: ((_ valueChange: Float?, _ didSelect:Bool,_ selectedLayerGeoJSON: GISLayer?) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        adapter = GISSideMenuAdapter(tableView, didChange: didChange)
        adapter.trwslayerData = self.trwslayerData
        adapter.selectedGISLayer = selectedGISLayer
        
        cstTableViewLeading.constant = (UIScreen.main.bounds.width * -0.7)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        cstTableViewLeading.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.6)
            self.view.layoutIfNeeded()
        }
    }
    
    
    init(valueDidChange didChange: @escaping (_ valueChange: Float?, _ didSelect:Bool,_ selectedLayerGeoJSON: GISLayer?) -> Void) {
        let bundle = Bundle(for: type(of: self))
        super.init(nibName: "GISSideMenuController", bundle: bundle)
        
        self.modalPresentationStyle = .overFullScreen
        self.modalTransitionStyle = .crossDissolve
        self.didChange = didChange
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    deinit {
        print("[SideMenuController] deinit")
    }
    
    
    @IBAction func didTapDoneButton(_ sender: UIButton) {
        
        cstTableViewLeading.constant = (UIScreen.main.bounds.width * -0.7)
        UIView.animate(withDuration: 0.3, animations: {
            self.view.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
            self.view.layoutIfNeeded()
        }) { (_) in
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    
    
    
}
