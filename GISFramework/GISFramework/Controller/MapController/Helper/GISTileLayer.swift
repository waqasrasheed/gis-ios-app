//
//  GISTileLayer.swift
//  GISFramework
//
//  Created by Waqas Rasheed on 17/02/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import Foundation
import GoogleMaps

class GISTileLayer: Equatable {
    
    var id: Int!
    var layer: GMSTileLayer!
    
    
    init() {
        //super.init()
    }
    
    //    let TILE_SIZE = 256.0
    //    let MINIMUM_ZOOM = 0
    //    let MAXIMUM_ZOOM = 25
    //    let TILE_CACHE = "TILE_CACHE"
    
    struct BBox{
        let left: Double;
        let  right: Double;
        let  bottom: Double;
        let  top: Double;
    }
    func bboxFromXYZ(_ x: UInt, y: UInt, z: UInt) -> BBox {
        
        let bbox = BBox(left: mercatorXofLongitude(lon: xOfColumn(x,zoom: z)), right: mercatorXofLongitude(lon: xOfColumn(x+1,zoom: z)), bottom: mercatorYofLatitude(lat: yOfRow(row: y+1,zoom: z)), top: mercatorYofLatitude(lat: yOfRow(row: y,zoom: z)))
        
        return bbox
    }
    
    func xOfColumn(_ column: UInt, zoom: UInt) -> Double {
        let x = Double(column)
        let z = Double(zoom)
        return x / pow(2.0, z) * 360.0 - 180
    }
    
    func yOfRow(row: UInt, zoom: UInt) -> Double {
        let y = Double(row);
        let z = Double(zoom);
        let n = Double.pi - 2.0 * Double.pi * y / pow(2.0, z);
        return 180.0 / Double.pi * atan(0.5 * (exp(n) - exp(-n)));
    }
    
    func mercatorXofLongitude(lon: Double) -> Double {
        return  lon * 20037508.34 / 180;
    }
    func mercatorYofLatitude(lat: Double) -> Double {
        var y = log(tan((90 + lat) * Double.pi / 360)) / (Double.pi / 180);
        y = y * 20037508.34 / 180;
        return y
    }
    
    static func == (lhs: GISTileLayer, rhs: GISTileLayer) -> Bool {
        return lhs.id == rhs.id
    }
    
    static func != (lhs: GISTileLayer, rhs: GISTileLayer) -> Bool {
        return lhs.id != rhs.id
    }
    
}

struct GISQueryString {
    
    var layerType: GISBaseLayer.LayerType = .none
    var wmsServer = "https://was.dev.meeraspace.com/geoserver/wms?"
    var wfsServer: String? = "https://was.dev.meeraspace.com/geoserver/wfs?"
    var layerOpacity: Float?
    
    var layerName = "filespace:osmalkhuwair"
    let ESPG = "3857"
    var ID = 7279
    var displayOrder: Int?
    var attribute: String? = "osm_id"
    
    var baseLayerURL: String? = "http://nsdig2gapps.ncsi.gov.om/arcgis1/rest/services/Geoportal/Education_Services/MapServer"
    
    init() {
        
    }
    
    init(baseLayerURL: String, id: Int,layerType: GISBaseLayer.LayerType) {
        
        self.baseLayerURL = baseLayerURL
        self.ID = id
        self.layerType = layerType
    }
    
    init(baseLayerURL: String,layerName: String, id: Int,layerType: GISBaseLayer.LayerType) {
        
        self.baseLayerURL = baseLayerURL
        self.ID = id
        self.layerType = layerType
        self.layerName = layerName
    }
    
    init(wmsServer: String, wfsServer: String?, layerName: String, id: Int, attribute: String?,layerType: GISBaseLayer.LayerType) {
        
        self.wmsServer = wmsServer
        self.wfsServer = wfsServer
        self.layerName = layerName
        self.ID = id
        self.attribute = attribute
        self.layerType = layerType
    }
    
    func baseLayerURLString() -> String {
        
        let qs = self
        
        if qs.layerType == .ESRI {
            
            let esriPath = "\(qs.baseLayerURL!)/export?F=image&FORMAT=PNG32&TRANSPARENT=true&LAYERS=show\(qs.layerName)&SIZE=1233,1477&BBOXSR=3857&IMAGESR=3857&DPI=180"
            
            return esriPath
            //drawLayerOnMapWithPath(path: esriPath,layerID: qs.ID,layerType: qs.layerType,layerOpacity: nil)
            
        }
        else if qs.layerType == .TileXYZ {
            
            let tileXYZPath  = qs.baseLayerURL!.replacingOccurrences(of: "mt{0-3}", with: "mt0")
            //drawLayerOnMapWithPath(path: tileXYZPath,layerID: qs.ID,layerType: qs.layerType,layerOpacity: nil)
            
            return tileXYZPath
        }
        
        return "Type Did not matched"
    }
    
    func wmsOrWfsURLString() -> (wmsLayer: String, wfsLayer: String) {
        
        let ESPG = "3857"
        
        if self.layerType == .none {
            
            let wsmPath = "\(self.wmsServer)SERVICE=WMS&VERSION=1.30&REQUEST=GetMap&layers=\(self.layerName)&width=256&height=256&srs=EPSG:\(ESPG)&format=image/png8&transparent=true&id=\(self.ID)&LayerName=\(self.layerName)&Attribute=\(self.attribute ?? "")"
            
            
            let wfsPath = "\(self.wmsServer)SERVICE=WMS&VERSION=1.30&REQUEST=GetMap&layers=\(self.layerName)&width=256&height=256&srs=EPSG:\(ESPG)&format=image/png8&transparent=true&id=\(self.ID)&LayerName=\(self.layerName)&Attribute=\(self.attribute ?? "")"
            
            return (wsmPath,wfsPath)
            
        }
        
        return ("","")
    }
}
