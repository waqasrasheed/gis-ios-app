//
//  HelperGISLayer.swift
//  GISFramework
//
//  Created by Muhammad Arslan Khalid on 14/02/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit
import SwiftyJSON

class HelperGISLayer: NSObject {
    
    static var shared = HelperGISLayer()
    var mapCashe: [GISLayerModel] = []
    var activeMap: [GISLayerModel]?
    
    var serverCashe: [GISServerModel] = []
    var dataSetCashe: [GISLayer] = []
    
}


extension HelperGISLayer
{
    func getTrwslLayerData(baseURL:String,completion:@escaping (_ result:[GISLayerModel]?,_ error:GISErrorModel?) -> Void, tokenRefresh:@escaping (_ isSuccess:Bool)->Void)
    {
        Network.asyncCallRestFullApi(baseUrl: baseURL, pathUrl: "/service/map/active?activeOnly=true", parameter: nil, serviceMethod: GISServiceType.GET, success: { (json) in
            //self.trwslayerData = GISLayerModel(json: json["data"][0])
            
            if let arr = json["data"].array {
                
                let data = arr.map({GISLayerModel(json: $0)})
                
                self.activeMap = data
                completion(data, nil)
                return;
            }
            
            completion(nil, nil)
        }, failure: { (error) in
            completion(nil,error)
        }) { (isSuces) in
            tokenRefresh(isSuces)
        }
    }
    
    func getMapsList(baseURL:String,pathURL:String,completion:@escaping(_ result:[GISLayerModel]?,_ error:GISErrorModel?) -> Void,tokenRefresh:@escaping (_ isSuccess:Bool)->Void)
    {
        Network.asyncCallRestFullApi(baseUrl: baseURL, pathUrl: pathURL, parameter: nil, serviceMethod: GISServiceType.GET, success: { (json) in
            
            var result = [GISLayerModel]()
            if let mapData = json["data"].array
            {
                result = mapData.map({ (mapJson) -> GISLayerModel in
                    GISLayerModel(json: mapJson)
                })
            }
            
            self.mapCashe = result
            
            completion(result,nil)
        }, failure: { (error) in
            completion(nil,error)
        }) { (isSuces) in
            tokenRefresh(isSuces)
        }
    }
    
    func getServersData(baseURL:String,pathURL:String,completion:@escaping(_ result:[GISServerModel]?,_ error:GISErrorModel?) -> Void,tokenRefresh:@escaping (_ isSuccess:Bool)->Void)
    {
        Network.asyncCallRestFullApi(baseUrl: baseURL, pathUrl: pathURL, parameter: nil, serviceMethod: GISServiceType.GET, success: { (json) in
            
            var result = [GISServerModel]()
            if let mapData = json["data"].array
            {
                result = mapData.map({ (serverJson) -> GISServerModel in
                    GISServerModel(json: serverJson)
                })
            }
            
            self.serverCashe = result
            completion(result,nil)
        }, failure: { (error) in
            completion(nil,error)
        }) { (isSuces) in
            tokenRefresh(isSuces)
        }
    }
    
    func getDataSets(baseURL:String,pathURL:String,completion:@escaping(_ result:[GISLayer]?,_ error:GISErrorModel?) -> Void,tokenRefresh:@escaping (_ isSuccess:Bool)->Void)
    {
        Network.asyncCallRestFullApi(baseUrl: baseURL, pathUrl: pathURL, parameter: nil, serviceMethod: GISServiceType.GET, success: { (json) in
            
            var result = [GISLayer]()
            if let mapData = json["data"].array
            {
                result = mapData.map({ (mapLayerJson) -> GISLayer in
                    GISLayer(json: mapLayerJson)
                })
            }
            
            self.dataSetCashe = result
            completion(result,nil)
        }, failure: { (error) in
            completion(nil,error)
        }) { (isSuces) in
            tokenRefresh(isSuces)
        }
    }
    
    func getGeoJSON(baseURL:String,pathURL:String,parameter:[String:Any]? ,completion:@escaping(_ result:JSON?,_ error:GISErrorModel?) -> Void,tokenRefresh:@escaping (_ isSuccess:Bool)->Void)
    {
        
        Network.asyncCallRestFullApi(baseUrl: baseURL, pathUrl: pathURL, parameter: parameter, serviceMethod: GISServiceType.POST, success: { (json) in
            
            print(json)
            
            
            completion(json,nil)
            
        }, failure: { (error) in
            completion(nil,error)
        }) { (isSuces) in
            tokenRefresh(isSuces)
        }
    }
    
    func getUserProfile(completion:@escaping(_ result:GISProfileModel?,_ error:GISErrorModel?) -> Void)
    {
        var parameter = [String:Any]()
        parameter["query"] = Q_profile
        
        Network.asyncCallAPIForJSONResult(WithBaseUrl: GISFramework.gisProfileGraphQLURL, WithQueryParameter: parameter, WithDelegate: nil) { (json, error) in
            
            if let josn = json
            {
                if josn["data"]["me"]["userProfile"].dictionary != nil {
                    
                    let profile = GISProfileModel(json: josn["data"]["me"]["userProfile"])
                    completion(profile, nil)
                    return;
                }
                
                completion(nil,GISErrorModel(errorTitle: "", errorDescp: "Profile not found", error: nil))
            }
            else
            {
                completion(nil,error)
            }
            
        }
    }
}
