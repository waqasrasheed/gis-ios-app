//
//  GISMamVCExtension.swift
//  GISFramework
//
//  Created by Waqas Rasheed on 02/03/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import MBProgressHUD
import SDWebImage

//MARK:- Login Method
extension GISMapViewController {
    
    //private func loginInApp(userName:String = "usman.aleem@targetofs.com", password:String = "Helpdesk@111")
    //private func loginInApp(userName:String = "targetmohsin@gmail.com", password:String = "password")
    
    func loginInApp(userName:String = "usman.aleem@targetofs.com", password:String = "Helpdesk@111")
    {
        
        guard  Network.oauthswift != nil else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        CommonNetworkApis.loginInAppToGetAccessToken(ouathInfo: Network.oauthswift, userName: userName, password: password) { (accessToken, refreshAccessToken, error) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            guard error == nil else {
                GISAlert.showServerErrors(controller: self, errorModel: error!)
                return
            }
            
            self.getMapsList()
            self.getDataSets()
            self.getMapServers()
        }
    }
}

// MARK: - Tap Handlers
extension GISMapViewController {
    
    func getMapsList() {
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        HelperGISLayer.shared.getMapsList(baseURL: self.baseURL, pathURL: "/service/map/active", completion: { (result, error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            
            guard let result = result else {
                GISAlert.showServerErrors(controller: self, errorModel: error!)
                return
            }
            print(result.count)
            //self.adapterMaps.data = result
            
            self.getActiveLayer()
            
        }) { (isSuccess) in
            
        }
    }
    
    func  getMapServers() {
        HelperGISLayer.shared.getServersData(baseURL: self.baseURL, pathURL: "/service/entity/MapServer", completion: { (result, error) in
            guard let _ = result else {
                GISAlert.showServerErrors(controller: self, errorModel: error!)
                return
            }
            //self.adapterServers.data = result
            
        }) { (isSucces) in
            
        }
    }
    
    func getDataSets() {
        HelperGISLayer.shared.getDataSets(baseURL: self.baseURL, pathURL: "/service/entity/MapLayer", completion: { (result, error) in
            guard let _ = result else {
                GISAlert.showServerErrors(controller: self, errorModel: error!)
                return
            }
            //self.adapterDataSets.data = result
            
        }) { (isSucces) in
            
        }
    }
    
    
    private func getActiveLayer()
    {
        guard  AppUtility.hasValidText(Network.accessToken) else {
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        HelperGISLayer.shared.getTrwslLayerData(baseURL: self.baseURL, completion: { (result, error) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            guard let error = error else {
                
                if result?.count ?? 0 > 0 {
                    
                    let matches = HelperGISLayer.shared.mapCashe.filter { (model) -> Bool in
                        
                        return result!.contains(model)
                    }
                    
                    let activeModel = matches.map { (model) -> GISLayerModel in
                        
                        model.isActive = true
                        return model
                    }
                    
                    if activeModel.count > 0 {
                        self.layerData = activeModel[0]
                        self.loadData()
                    }
                    
                    self.getProfile()
                }
                
                
                //self.adapterMaps.reloadData()
                return
            }
            
            GISAlert.showServerErrors(controller: self, errorModel: error)
            
        }) { (isSuccess) in
            
        }
    }
    
    
    private func getProfile()
    {
        guard  AppUtility.hasValidText(Network.accessToken) else {
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        HelperGISLayer.shared.getUserProfile { (profile, errorModel) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            if errorModel == nil {
                
                if let legendURL = profile!.photo.imageURLWithToken {
                    
                    self.profileImage.sd_setImage(with: legendURL, placeholderImage: UIImage(named: "wmsHolder"), options: SDWebImageOptions.continueInBackground) { (img, error, cache, url) in
                        
                    }
                }
            }
            else {
                
                GISAlert.showServerErrors(controller: self, errorModel: errorModel!)
            }
        }
    }
}

// MARK:- POP UP MENU DELEGATE
extension GISMapViewController: PopMenuViewControllerDelegate {
    
    @objc func btnMenuPressed(_ sender: UIButton) {
        
        let textColor = UIColor.colorWithRGB(99.0, 99.0, 99.0, opacity: 1.0)
        let controller = PopMenuViewController(sourceView: sender,actions: [
            PopMenuDefaultAction(title: "Draw Polygon", image: nil,color: textColor),
            //PopMenuDefaultAction(title: "Complete Polygon", image: nil,color: textColor)
        ])
        
        // Customize appearance
        controller.appearance.popMenuFont = UIFont.systemFont(ofSize: 13)
        
        controller.appearance.popMenuBackgroundStyle = .none()
        // Configure options
        controller.shouldDismissOnSelection = true
        controller.appearance.popMenuCornerRadius = 3;
        controller.appearance.popMenuColor.backgroundColor = .solid(fill: .white)
        
        
        controller.delegate = self
        
        controller.didDismiss = { selected in
            print("Menu dismissed: \(selected ? "selected item" : "no selection")")
        }
        
        
        present(controller, animated: true, completion: nil)
        
    }
    
    public func popMenuDidSelectItem(_ popMenuViewController: PopMenuViewController, at index: Int) {
        
        if index == 0 {
            self.drawPolygonPressed()
        }
    }
    
    @IBAction func btnCompletePolygonePressed(_ sender: UIButton) {
        
        self.drawEndPolygone()
    }
    
    
}
