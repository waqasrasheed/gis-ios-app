//
//  GISMapViewController.swift
//  GISFramework
//
//  Created by Waqas Rasheed on 12/02/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit
import GoogleMaps
import MBProgressHUD
import GoogleMapsUtils
import SwiftyJSON

public class GISMapViewController: UIViewController {
    
    @IBOutlet weak var topBar: GISTopBar!
    @IBOutlet weak var navHeight: NSLayoutConstraint!
    var isHidderTopBar: Bool = false;
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var completePolygoneContainer: UIView!
    
    var url:URL!
    var baseURL:String!
    var layerData: GISLayerModel! {
        didSet {
            self.topBar.lblNavHeading.text = self.layerData.name
            self.lblTitle.text = self.layerData.name
        }
    }
    var casheLayer = [GISTileLayer]()
    
    var selectedGISLayer: GISLayer? {
        didSet {
            if self.selectedGISLayer == nil {
                self.removeGeometery()
                self.removeGMSPolylinePath()
            }
        }
    }
    var plotPoints:[CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
    private var plotPointType:DrawPlotPointsEnum!
    private var arrPolyLines = [GMSPolyline]()
    private var arrCircles =  [GMSCircle]()
    private var renderer: GMUGeometryRenderer?
    private var canDrawPolygone: Bool = false
    private var propertyModel: [GISPropertyModel] = []
    
    public init() {
        
        let bundle = Bundle(for: GISMapViewController.self)
        super.init(nibName: "GISMapViewController", bundle: bundle)
        
        self.baseURL = GISFramework.gisBaseURL
        Network.oauthswift = GISFramework.oauthswift
        self.loginInApp()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 01) {
            //self.getDataForLayers()
            //HelperTRWMSLayer.shared.trwslayerData = self.layerData
            //self.addTiles()
        }
    }
    
    public init(baseURL:String,oauthswift: OAuth2Swift) {
        
        let bundle = Bundle(for: GISMapViewController.self)
        super.init(nibName: "GISMapViewController", bundle: bundle)
        
        self.baseURL = baseURL
        Network.oauthswift = oauthswift
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 01) {
            self.setTopBarGUI()
            //self.getDataForLayers()
            
        }
    }
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setTopBarGUI()
    {
        if(isHidderTopBar == true)
        {
            navHeight.constant = 0;
            topBar.isHidden = true;
            return;
        }
        
        self.topBar.lblNavHeading.text = "Oman Map"
        self.topBar.lblNavHeading.isHidden = true
        self.topBar.btnBack.addTarget(self, action: #selector(self.btnBackPressed(_:)), for: UIControl.Event.touchUpInside)
        self.topBar.btnBack.isHidden = true
        self.topBar.btnRight.isHidden = false
        
        let bundle = Bundle(for: GISMapViewController.self)
        self.topBar.btnRight.setImage(UIImage(named: "gisthreeDotsBg", in: bundle, compatibleWith: nil) , for: .normal)
        //self.topBar.btnRight.setBackgroundImage(UIImage(named: "gfs_polygon"), for: .normal)
        self.topBar.btnRight.addTarget(self, action: #selector(self.btnMenuPressed(_:)), for: UIControl.Event.touchUpInside)
        AppUtility.setNavHeightForIPhoneX(navHightRef: self.navHeight)
        
    }
    
    @IBAction func btnBackPressed(_ sender: Any) {
        
        if presentingViewController == nil {
            self.navigationController?.popViewController(animated: true);
        }
        else {
            self.dismiss(animated: true, completion: nil);
        }
    }
    
    
    @IBOutlet weak var mpView: GISMapView!
    
    var layerMap: GMSMapView {
        return mpView.TRMapView
    }
    
    public override func loadView() {
        super.loadView()
        
    }
    override public func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        layerMap.delegate = self
        self.plotPointType = nil
        //self.loginInApp()
        assert(self.baseURL != nil)
        
        self.setTopBarGUI()
        
        
        //testGeoJSON()
        
        
        //self.layerMap.padding = UIEdgeInsets(top: 300, left: 0, bottom: 0, right: 0)
        
        self.currentLocationButtonEnabled()
        self.cameraMoveToCurrentLocation(mapView: self.layerMap)
        
    }
    
    func loadData() {
        
        self.clearMap()
        canDrawPolygone = false
        self.plotPointType = nil
        self.selectedGISLayer = nil
        self.completePolygoneContainer.isHidden = true
        self.casheLayer.removeAll()
        //layerMap.camera = GMSCameraPosition(latitude: 23.5880, longitude: 58.3829, zoom: 12)
        //layerMap.camera = GMSCameraPosition(latitude: -40.9006, longitude: 174.8860, zoom: 7) // newzeland
        drawDefultLayer()
        
        //self.setZoom(minX: self.layerData.minX, minY: self.layerData.minY, maxX: self.layerData.maxX, maxY: self.layerData.maxY)
    }
    
    func testGeoJSON() {
        
        let bundle = Bundle(for: GISMapViewController.self)
        let path = bundle.path(forResource: "GeoJSON_Sample", ofType: "geojson")
        let url = URL(fileURLWithPath: path!)
        let geoJsonParser = GMUGeoJSONParser(url: url)
        geoJsonParser.parse()
        
        let renderer = GMUGeometryRenderer(map: self.layerMap, geometries: geoJsonParser.features)
        
        renderer.render()
    }
    
    @IBAction func didTapBackButton(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func didTapMenuButton(_ sender: UIButton) {
        
        let controller = GISSideMenuController { (sliderValue, didSelect,selectedLayerGeoJSON) in
            
            print("Value Did Change")
            
            if selectedLayerGeoJSON != nil {
                
                self.removeGeometery()
                self.removeGMSPolylinePath()
                self.selectedGISLayer = selectedLayerGeoJSON
            }
            else {
                
                if didSelect == true {
                    
                    self.selectedGISLayer = selectedLayerGeoJSON
                   
                    //self.removeUnselectLayer()
                    //self.removeUnselectBaseLayer()
                    self.clearMap()
                    
                    var queryObj = [GISQueryString]()
                    
                    let qWMSL = self.drawWMSLayer()
                    
                    queryObj.append(contentsOf: qWMSL)
                    
                    if didSelect == true && sliderValue == nil {
                        
                        let qBL = self.drawBaseLayer()
                        
                        queryObj.append(contentsOf:qBL)
                        
                    }
                    
                    
                    if queryObj.count > 0 {
                        self.drawAllLayers(queryObjecst: queryObj)
                    }
                }
            }
            
        }
        
        guard self.layerData != nil else {
            GISAlert.showAlert(title: "", descp: "Failed to fetch the layer data", controller: self)
            return
        }
        
        controller.trwslayerData = self.layerData
        controller.selectedGISLayer = self.selectedGISLayer
        self.present(controller, animated: true, completion: nil)
    }
    
    func removeUnselectLayer() {
        
        let unselectedLayers = self.layerData.getUnSelectedLayers()
        let casheIDS = casheLayer.map({$0.id})
        
        if unselectedLayers.count > 0 {
            
            for n in unselectedLayers {
                
                if casheIDS.contains(n.id)  {
                    
                    let arrlayer = casheLayer.filter({$0.id == n.id})
                    
                    for n in arrlayer {
                        
                        n.layer.map = nil
                        n.layer.clearTileCache()
                    }
                    
                    break
                }
            }
        }
        
    }
    
    func removeUnselectBaseLayer() {
        
        let unselectedBaseLayers = self.layerData.getUnSelectedBaseLayers()
        let casheIDS = casheLayer.map({$0.id})
        
        if unselectedBaseLayers.count > 0 {
            
            for n in unselectedBaseLayers
            {
                if casheIDS.contains(n.id)  {
                    
                    let arrlayer = casheLayer.filter({$0.id == n.id})
                    
                    for n in arrlayer {
                        
                        n.layer.map = nil
                        n.layer.clearTileCache()
                    }
                    
                    break
                }
            }
        }
    }
    
    func drawDefultLayer() {
        
        self.layerMap.clear()
        
        var queryObj = [GISQueryString]()
        
        let qWMSL = self.drawWMSLayer()
        
        queryObj.append(contentsOf: qWMSL)
        
        
        let qBL = self.drawBaseLayer()
        
        queryObj.append(contentsOf:qBL)
        
        
        
        if queryObj.count > 0 {
            self.drawAllLayers(queryObjecst: queryObj)
        }
    }
    
    func drawWMSLayer() -> [GISQueryString] {
        
        let mainSelectedLayer = self.layerData.selectedLayers()
        
        var queryObj = [GISQueryString]()
        
        for n in mainSelectedLayer {
            
            if n.validate() {
                
                let clayer = self.casheLayer.filter { (model) -> Bool in
                    return model.id == n.id
                }
                
                if clayer.count > 0 { //wms
                    
                    clayer[0].layer.opacity = n.sliderOpacityValue
                    self.drawCasheLayers(chLayers: clayer)
                    
                    if clayer.count > 1 { //wfs
                        clayer[1].layer.opacity = n.sliderOpacityValue
                        self.drawCasheLayers(chLayers: clayer)
                    }
                }
                else {
                    var obj = GISQueryString(wmsServer: n.mapServer!.url!, wfsServer: n.mapServer!.featureServerURL, layerName: n.name!, id: n.id!, attribute: nil,layerType: .none)
                    obj.layerOpacity = n.sliderOpacityValue
                    obj.displayOrder = n.displayOrder
                    
                    queryObj.append(obj)
                }
            }
            
        }
        
        return queryObj
    }
    
    func drawBaseLayer() -> [GISQueryString] {
        
        let baseSelectedLayer = self.layerData.selectedBaseLayers()
        
        var queryObj = [GISQueryString]()
        
        for n in baseSelectedLayer {
            
            if n.validate() {
                
                let clayer = self.casheLayer.filter { (model) -> Bool in
                    return model.id == n.id
                }
                
                if clayer.count > 0 {
                    
                    self.drawCasheLayers(chLayers: clayer)
                }
                else {
                    
                    if n.layerType == GISBaseLayer.LayerType.TileXYZ {
                        
                        var obj = GISQueryString(baseLayerURL: n.url!, id: n.id!,layerType: .TileXYZ)
                        obj.displayOrder = n.displayOrder
                        queryObj.append(obj)
                    }
                    else if n.layerType == GISBaseLayer.LayerType.ESRI {
                        
                        var obj = GISQueryString(baseLayerURL: n.url!, layerName: n.name!, id: n.id!,layerType: .ESRI)
                        obj.displayOrder = n.displayOrder
                        queryObj.append(obj)
                    }
                    
                }
            }
            
        }
        
        return queryObj
    }
    
    deinit {
        //self.layerData.unSelectedLayers()
        //self.layerData.unSelectedBaseLayers()
        
    }
}
extension GISMapViewController {
    
    func setZoom(minX: Double?,minY:Double?, maxX: Double?, maxY:Double?) {
        
        if minX != nil && minY != nil && maxX != nil && maxY != nil {
            
            let bound = GMSCoordinateBounds(coordinate: CLLocationCoordinate2D(latitude: minX!, longitude: minY!), coordinate: CLLocationCoordinate2D(latitude: maxX!, longitude: maxY!))
            
            let update = GMSCameraUpdate.fit(bound, withPadding: 1)
            self.layerMap.animate(with: update)
            
            //self.layerMap.animate(to: GMSCameraPosition(latitude: minX!, longitude: minY!, zoom: 4))
        }
        
    }
    
    
    func currentLocationButtonEnabled() {
        
        self.layerMap.isMyLocationEnabled = true
        self.layerMap.settings.myLocationButton = true
    }
    
    public func didTapMyLocationButton(for mapView: GMSMapView) -> Bool {
        
        print("Location Button Pressed")
        
        self.cameraMoveToCurrentLocation(mapView: mapView)
        
        return true
    }
    
    private func cameraMoveToCurrentLocation(mapView: GMSMapView) {
        
        if let c = mapView.myLocation {
            
            //mapView.camera = GMSCameraPosition(target: c.coordinate, zoom: 13, bearing: 0, viewingAngle: 0)
            self.setZoomLevel(location: c,zoom: 13)
            
        } else {
            GISLocationManager.shared.getCurrentLocation { (location) in
                
                self.setZoomLevel(location: location,zoom: 13)
            }
        }
        
    }
    
    private func setZoomLevel(location: CLLocation, zoom: Float = 16) {
        
        let cameraPosition = GMSCameraPosition(target: location.coordinate, zoom: zoom, bearing: 0, viewingAngle: 0)
        self.layerMap.animate(to: cameraPosition)
    }
}

extension GISMapViewController {
    
    func drawAllLayers(queryObjecst: [GISQueryString]) {
        
        for n in 0..<queryObjecst.count {
            
            let qs = queryObjecst[n]
            
            if qs.layerType == .ESRI {
                
                drawLayerOnMapWithPath(path: qs.baseLayerURLString(), queryObjecst: qs)
                
            }
            else if qs.layerType == .TileXYZ {
                
                drawLayerOnMapWithPath(path: qs.baseLayerURLString(), queryObjecst: qs)
            }
            else if qs.layerType == .none {
                
                drawLayerOnMapWithPath(path: qs.wmsOrWfsURLString().wmsLayer, queryObjecst: qs)
                
                drawLayerOnMapWithPath(path: qs.wmsOrWfsURLString().wfsLayer, queryObjecst: qs)
            }
            
        }
        
    }
    
    func drawLayerOnMapWithPath(path: String,queryObjecst: GISQueryString) {
        
        let tileOverlay = GISTileLayer()
        
        let urls = { (x: UInt, y: UInt, zoom: UInt) -> URL in
            
            let bbox = tileOverlay.bboxFromXYZ(x, y: y, z: zoom)
            let BOX = "\(bbox.left),\(bbox.bottom),\(bbox.right),\(bbox.top)"
            
            var newPath = ""
            
            if queryObjecst.layerType == .TileXYZ { //base layer
                
                //"https://mt{0-3}.google.com/vt/lyrs=r&x={x}&y={y}&z={z}"
                var p  = path.replacingOccurrences(of: "{x}", with: "\(x)")
                p  = p.replacingOccurrences(of: "{y}", with: "\(y)")
                p  = p.replacingOccurrences(of: "{z}", with: "\(zoom)")
                
                newPath = p
            }
            else if queryObjecst.layerType == .ESRI { //base layer
                //"http://nsdig2gapps.ncsi.gov.om/arcgis1/rest/services/Geoportal/Education_Services/MapServer/export?F=image&FORMAT=PNG32&TRANSPARENT=true&LAYERS=showEducation%20Services&SIZE=1233%2C1477&BBOX=\(bbox.left),\(bbox.bottom),\(bbox.right),\(bbox.top)&BBOXSR=3857&IMAGESR=3857&DPI=180"
                
                newPath = path + "&BBOX=\(BOX)"
            }
            else if queryObjecst.layerType == .none {
                
                newPath = path + "&BBOX=\(BOX)"
            }
            
            
            
            return URL(string: newPath.encodeUrl!)!
        }
        
        
        // Create the GMSTileLayer
        let layer = GMSURLTileLayer(urlConstructor: urls)
        
        if let zIndex = queryObjecst.displayOrder {
            layer.zIndex = Int32(zIndex)
        }
        
        if queryObjecst.layerType == .none && queryObjecst.layerOpacity != nil {
            layer.opacity = queryObjecst.layerOpacity!
        }
        
        
        let cusLayer = GISTileLayer()
        cusLayer.id = queryObjecst.ID
        cusLayer.layer = layer
        
        layer.map = self.layerMap
        
        casheLayer.append(cusLayer)
        /*if !casheLayer.contains(cusLayer) {
         }*/
    }
    
    func drawCasheLayers(chLayers: [GISTileLayer]) {
        
        if chLayers.count > 0 {
            
            chLayers[0].layer.map = self.layerMap
        }
        
        
        /*for layer_ in chLayers {
         
         layer_.map = self.layerMap
         }*/
    }
    
}

extension GISMapViewController {
    
    //    func getCordinatesOfRectAnnotations(annotationsArray:MKAnnotation) ->[CLLocationCoordinate2D]{
    //        let minX : Double = annotationsArray.map({MKMapPointForCoordinate($0.coordinate).x}).min()!
    //        let minY : Double = annotationsArray.map({MKMapPointForCoordinate($0.coordinate).y}).min()!
    //        let maxX : Double = annotationsArray.map({MKMapPointForCoordinate($0.coordinate).x}).max()!
    //        let maxY : Double = annotationsArray.map({MKMapPointForCoordinate($0.coordinate).y}).max()!
    //
    //        var result : [CLLocationCoordinate2D] = []
    //        result.append(MKCoordinateForMapPoint(MKMapPoint(x: minX, y: minY)))
    //        result.append(MKCoordinateForMapPoint(MKMapPoint(x: maxX, y: minY)))
    //        result.append(MKCoordinateForMapPoint(MKMapPoint(x: maxX, y: maxY)))
    //        result.append(MKCoordinateForMapPoint(MKMapPoint(x: minX, y: maxY)))
    //        return result
    //    }
}
//MARK:- utility Methods
extension GISMapViewController {
    
    
    private func getGeoJSONForPolygone()
    {
        guard  AppUtility.hasValidText(Network.accessToken), self.selectedGISLayer != nil else {
            return
        }
        
        var polyGoneStr = ""
        for n in 0..<self.plotPoints.count {
            
            let coord = self.plotPoints[n]
            
            if n == self.plotPoints.count - 1 {
                
                polyGoneStr.append("\(coord.longitude)" + " " + "\(coord.latitude)")
            }
            else {
                polyGoneStr.append("\(coord.longitude)" + " " + "\(coord.latitude)" + ",")
            }
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        //polyGoneStr = "172.54131317138675 -40.85582496460844,170.26036262512207 -43.59487621931377,170.73200225830078 -44.61417834290233,173.3181667327881 -42.295595824496424,172.54131317138675 -40.85582496460844"
        
        let csrCode = "4326"
        var inputPram = [String: String]()
        inputPram["wfsUrl"] = self.selectedGISLayer?.mapServer?.featureServerURL
        inputPram["layerName"] = self.selectedGISLayer?.featureType
        inputPram["wmsLayerName"] = self.selectedGISLayer?.name
        inputPram["crsCode"] = csrCode
        inputPram["catalog"] = self.selectedGISLayer?.featureCatalog?.name
        inputPram["filter"] = "INTERSECTS(THE_GEOM, SRID=\(csrCode);POLYGON((\(polyGoneStr))))"
        
        HelperGISLayer.shared.getGeoJSON(baseURL: self.baseURL, pathURL: "/spatial/api/v1/wfs/readjson", parameter: inputPram, completion: { (result, error) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            guard let error = error else {
                
                if let arr = result!["features"].array {
                    
                    self.propertyModel = arr.map({GISPropertyModel(json: $0["properties"])})
                    
                    if self.propertyModel.count > 0 {
                        
                        let controller = GISPropertiesVC(properties: self.propertyModel)
                        self.present(controller, animated: true, completion: nil)
                    }
                }
                
                do {
                    let data = try result?.rawData()
                    
                    self.drawGeoJSONonMap(jsonData: data!)
                }
                catch {
                }
                
                return
            }
            
            GISAlert.showServerErrors(controller: self, errorModel: error)
            
        }) { (isSuccess) in
            
        }
    }
}

extension GISMapViewController {
    
    func addTiles() {
        // Implement GMSTileURLConstructor
        let tileOverlay = GISTileLayer()
        
        let urls = { (x: UInt, y: UInt, zoom: UInt) -> URL in
            
            let bbox = tileOverlay.bboxFromXYZ(x, y: y, z: zoom)
            let BOX = "\(bbox.left),\(bbox.bottom),\(bbox.right),\(bbox.top)"
            
            //let path = "https://demoreach.digitalenergycloud.com/geoserver/wfs?service=WMS&version=1.1.1&request=GetMap&layers=filespace:OmanRoadWGS&bbox=\(bbox.left),\(bbox.bottom),\(bbox.right),\(bbox.top)&width=256&height=256&srs=EPSG:900913&format=image/png&transparent=true"
            var path = "https://mt0.google.com/vt/lyrs=r&x=\(x)&y=\(y)&z=\(zoom)"
            
            path = "http://nsdig2gapps.ncsi.gov.om/arcgis1/rest/services/Geoportal/Education_Services/MapServer/export?F=image&FORMAT=PNG32&TRANSPARENT=true&LAYERS=showEducation%20Services&SIZE=1233,1477&BBOX=\(bbox.left),\(bbox.bottom),\(bbox.right),\(bbox.top)&BBOXSR=3857&IMAGESR=3857&DPI=180"
            
            path = "http://nsdig2gapps.ncsi.gov.om/arcgis1/rest/services/Geoportal/Education_Services/MapServer/export?F=image&FORMAT=PNG32&TRANSPARENT=true&LAYERS=showEducation Services&SIZE=1233,1477&BBOX=\(BOX)&BBOXSR=3857&IMAGESR=3857&DPI=180"
            
            return URL(string: path.encodeUrl!)!
        }
        // Create the GMSTileLayer
        let layer = GMSURLTileLayer(urlConstructor: urls)
        //layer.tileSize = 256 // its standard.
        //layer.map = nil
        layer.map = self.layerMap
    }
    
    
    //"https://demoreach.digitalenergycloud.com/geoserver/wfs?service=WMS&version=1.1.1&request=GetMap&layers=filespace:OmanRoadWGS&bbox=%f,%f,%f,%f&width=256&height=256&srs=EPSG:900913&format=image/png&transparent=true"
}


extension GISMapViewController: GMSMapViewDelegate {
    
    public func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        self.drawLinePointsOnTap(coordinate:coordinate)
    }
    
    
    public func mapView(_ mapView: GMSMapView, didLongPressAt coordinate: CLLocationCoordinate2D) {
        
        //self.drawEndPolygone()
        //self.drawLinePointsOnTap(coordinate:coordinate)
    }
}

extension GISMapViewController {
    
    func drawPolygonPressed() {
        
        if self.selectedGISLayer != nil {
            self.removeGeometery()
            self.removeGMSPolylinePath()
            self.completePolygoneContainer.isHidden = true
            self.plotPointType = .polygone
        }
        else {
            GISAlert.showAlert(title: "", descp: "Please select layer", controller: self)
        }
        
    }
    
    func drawLinePointsOnTap(coordinate:CLLocationCoordinate2D)
    {
        if !canDrawPolygone {
            
            if self.selectedGISLayer == nil {
                GISAlert.showAlert(title: "", descp: "Please select layer", controller: self)
            }
            else if self.plotPointType == nil {
                GISAlert.showAlert(title: "", descp: "Select Draw polygone", controller: self)
            }
            
            return
        }
        
        if self.plotPointType != nil
        {
            switch self.plotPointType! {
            case .singlePoint:
                self.plotPoints.append(coordinate)
                self.showCircleOnCoordinates(coordinates: coordinate)
                
                if plotPoints.count > 1
                {
                    //self.addDistanceLabelOnMapWithCoordinates(lhs: self.plotPoints[self.plotPoints.count - 2], rhs: self.plotPoints[self.plotPoints.count - 1])
                }
                
                break;
            case .line:
                self.plotPoints.append(coordinate)
                self.showCircleOnCoordinates(coordinates: coordinate)
                if plotPoints.count > 1
                {
                    //self.addDistanceLabelOnMapWithCoordinates(lhs: self.plotPoints[self.plotPoints.count - 2], rhs: self.plotPoints[self.plotPoints.count - 1])
                    self.drawLineBetweenCoordinates(lhs: self.plotPoints[self.plotPoints.count - 2], rhs: self.plotPoints[self.plotPoints.count - 1])
                }
                
                break;
            case .polygone:
                self.plotPoints.append(coordinate)
                self.showCircleOnCoordinates(coordinates: coordinate)
                if plotPoints.count > 1
                {
                    //self.addDistanceLabelOnMapWithCoordinates(lhs: self.plotPoints[self.plotPoints.count - 2], rhs: self.plotPoints[self.plotPoints.count - 1])
                    self.drawLineBetweenCoordinates(lhs: self.plotPoints[self.plotPoints.count - 2], rhs: self.plotPoints[self.plotPoints.count - 1])
                    
                    if plotPoints.count > 2 {
                        self.completePolygoneContainer.isHidden = false
                    }
                }
                
                break;
                
            }
        }
    }
    
    func showCircleOnCoordinates(coordinates:CLLocationCoordinate2D)
    {
        let radiusCircle = GMSCircle.init(position: coordinates, radius: 1)
        radiusCircle.fillColor = .green
        radiusCircle.strokeColor = .red
        radiusCircle.map = self.layerMap
        self.arrCircles.append(radiusCircle)
        
    }
    
    func drawLineBetweenCoordinates(lhs:CLLocationCoordinate2D, rhs:CLLocationCoordinate2D)
    {
        let path = GMSMutablePath()
        path.add(lhs)
        path.add(rhs)
        let polyline = GMSPolyline(path: path)
        polyline.strokeColor = .red
        polyline.strokeWidth = 3.0
        polyline.map = self.layerMap
        
        self.arrPolyLines.append(polyline)
        
        
    }
    
    func drawEndPolygone() {
        
        if !canDrawPolygone {
            return
        }
        
        
        canDrawPolygone = false
        
        if self.plotPointType != nil
        {
            if plotPoints.count > 2 {
                
                let firstCoordinate = self.plotPoints[0]
                self.plotPoints.append(firstCoordinate)
                
                self.drawLineBetweenCoordinates(lhs: self.plotPoints[self.plotPoints.count - 2], rhs: self.plotPoints[self.plotPoints.count - 1])
                
                if plotPointType == DrawPlotPointsEnum.polygone
                {
                    self.drawLineBetweenCoordinates(lhs:  self.plotPoints[self.plotPoints.count - 1] , rhs: self.plotPoints.first!)
                    self.plotPointType = nil
                    self.getGeoJSONForPolygone()
                    
                    self.completePolygoneContainer.isHidden = true
                    
                }
            }
        }
    }
    
    func drawGeoJSONonMap(jsonData: Data) {
        
        let geoJsonParser = GMUGeoJSONParser(data: jsonData)
        
        geoJsonParser.parse()
        
        self.renderer = GMUGeometryRenderer(map: self.layerMap, geometries: geoJsonParser.features)
        self.renderer!.clear()
        self.renderer!.render()
    }
    
    func removeGMSPolylinePath ()
    {
        for n in self.arrPolyLines {
            n.map = nil
        }
        
        for n in self.arrCircles {
            n.map = nil
        }
        
        self.arrPolyLines.removeAll()
        self.arrCircles.removeAll()
        self.plotPoints.removeAll()
        
        //canDrawPolygone = true
        
        self.plotPointType = nil
        canDrawPolygone = self.selectedGISLayer == nil ? false : true
    }
    
    func removeGeometery() {
        
        self.renderer?.clear()
        self.renderer = nil
    }
    
    func clearMap() {
        print("clear all data source")
        self.removeGeometery()
        self.removeGMSPolylinePath()
        self.layerMap.clear()
    }
}
public extension NSObject {
    var theClassName: String {
        return NSStringFromClass(type(of: self))
    }
}
