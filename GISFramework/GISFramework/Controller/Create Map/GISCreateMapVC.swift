//
//  GISCreateMapVC.swift
//  GISFramework
//
//  Created by Waqas Rasheed on 02/03/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit
import MBProgressHUD
class GISCreateMapVC: UIViewController {
    
    @IBOutlet weak var topBar: GISTopBar!
    @IBOutlet weak var navHeight: NSLayoutConstraint!
    var isHidderTopBar: Bool = false;
    
    @IBOutlet var arrTxtFields:[UITextField]!
    @IBOutlet weak var txtName:UITextField!
    @IBOutlet weak var txtCRS:UITextField!
    @IBOutlet weak var txtDescp:UITextField!
    @IBOutlet weak var txtMinXoom:UITextField!
    @IBOutlet weak var txtMaxZoom:UITextField!
    @IBOutlet weak var txtMinLon:UITextField!
    @IBOutlet weak var txtMaxLon:UITextField!
    @IBOutlet weak var txtMinLat:UITextField!
    @IBOutlet weak var txtMaxLat:UITextField!
    @IBOutlet weak var btnRadioPublic:UIButton!
    @IBOutlet weak var btnRadioPrivate:UIButton!
    
    
    private var helper = HelperGISCreateMapVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setTopBarGUI()
        self.setData()
        self.setDelegate()
    }
    
    public init() {
        
        let bundle = Bundle(for: type(of: self))
        super.init(nibName: "GISCreateMapVC", bundle: bundle)
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setTopBarGUI()
    {
        if(isHidderTopBar == true)
        {
            navHeight.constant = 0;
            topBar.isHidden = true;
            return;
        }
        
        self.topBar.lblNavHeading.text = "Add Map"
        self.topBar.btnBack.isHidden = true
        AppUtility.setNavHeightForIPhoneX(navHightRef: self.navHeight)
        
    }
}

//MARK:- Btn Actions
extension GISCreateMapVC
{
    @IBAction func btnPublicPressed(_ sender:UIButton)
    {
        self.helper.createMapModel.isMapPrivate = false
        self.setData()
    }
    
    @IBAction func btnPrivatePressed(_ sender:UIButton)
    {
        self.helper.createMapModel.isMapPrivate = true
        self.setData()
    }
    
    @IBAction func btnClearPressed(_ sender:UIButton)
    {
        self.helper.clearData()
        self.setData()
    }
    
    @IBAction func btnCreateMapPressed(_ sender:UIButton)
    {
        if self.helper.checkValidation(arrTxtField: self.arrTxtFields, action: #selector(self.btnValidationMessagePressed(_:)), target: self)
        {
           // here we call api
            self.callApiToCreateMap()
        }
        
    }
    
    @objc func btnValidationMessagePressed(_ sender:UIButton)
    {
        GISAlert.showAlert(title: "", descp: sender.validationMsg, controller: self)
    }
    
    @objc func txtDidChanded(_ sender:UITextField)
    {
        switch sender {
        case self.txtName:
            self.helper.createMapModel.name = sender.text
            break
        case self.txtCRS:
            self.helper.createMapModel.crs = sender.text
            break
        case self.txtDescp:
            self.helper.createMapModel.descp = sender.text
            break
        case self.txtMinXoom:
            self.helper.createMapModel.minZoom = sender.text
            break
        case self.txtMaxZoom:
            self.helper.createMapModel.maxZoom = sender.text
            break
        case self.txtMinLon:
            self.helper.createMapModel.minLon = sender.text
            break
        case self.txtMaxLon:
            self.helper.createMapModel.maxLon = sender.text
            break
        case self.txtMinLat:
            self.helper.createMapModel.minLat = sender.text
            break
        case self.txtMaxLat:
            self.helper.createMapModel.maxLat = sender.text
            break
        default:
            break;
        }
    }
}

//MARK:- Helping Methods
extension GISCreateMapVC
{
    
    func setData()
    {
        self.txtName.text = self.helper.createMapModel.name
        self.txtCRS.text = self.helper.createMapModel.crs
        self.txtDescp.text = self.helper.createMapModel.descp
        self.txtMinXoom.text = self.helper.createMapModel.minZoom
        self.txtMaxZoom.text = self.helper.createMapModel.maxZoom
        self.txtMinLon.text = self.helper.createMapModel.minLon
        self.txtMaxLon.text = self.helper.createMapModel.maxLon
        self.txtMinLat.text = self.helper.createMapModel.minLat
        self.txtMaxLat.text = self.helper.createMapModel.maxLat
        
        if self.helper.createMapModel.isMapPrivate {
            self.btnRadioPrivate.setImage("gRadioButton_Selecged.png".bundleImage, for: UIControl.State.normal)
            self.btnRadioPublic.setImage("gRadioButton.png".bundleImage, for: UIControl.State.normal)
        }
        else {
            self.btnRadioPublic.setImage("gRadioButton_Selecged".bundleImage, for: UIControl.State.normal)
            self.btnRadioPrivate.setImage("gRadioButton".bundleImage, for: UIControl.State.normal)
        }
    }
    
    func setDelegate()
    {
        for field in self.arrTxtFields
        {
            field.delegate = self
            field.addTarget(self, action: #selector(self.txtDidChanded(_:)), for: UIControl.Event.editingChanged)
        }
    }
}

//MARK:- TextField Delegate Methods
extension GISCreateMapVC:UITextFieldDelegate
{
    
}

//MARK:- Network API's
extension GISCreateMapVC
{
    func callApiToCreateMap()
    {
        MBProgressHUD.showAdded(to: self.view, animated: true)
        self.helper.createMap { (error) in
            MBProgressHUD.hide(for: self.view, animated: true)
            guard let error = error else {
                GISAlert.showAlert(title: "", descp: "Map is created successfuly", controller: self)
                self.helper.clearData()
                self.setData()
                if let cont =  self.tabBarController?.viewControllers?[1] as? GISMainTabController
               {
                   cont.getMapsList(refreshCashe: true)
                }
                return
            }
            GISAlert.showServerErrors(controller: self, errorModel: error)
        }
    }
}
