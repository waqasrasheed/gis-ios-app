//
//  HelperGISCreateMapVC.swift
//  GISFramework
//
//  Created by Muhammad Arslan Khalid on 02/03/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit

class HelperGISCreateMapVC: NSObject {

    var createMapModel:CreateMapModel = CreateMapModel()
}


//MARK:- Helping Methods
 extension HelperGISCreateMapVC
{
    
    func clearData()
    {
        self.createMapModel.name = ""
        self.createMapModel.crs = "3875"
        self.createMapModel.descp = ""
        self.createMapModel.minZoom = "2"
        self.createMapModel.maxZoom = "28"
        self.createMapModel.minLon = ""
        self.createMapModel.maxLon = ""
        self.createMapModel.maxLat = ""
        self.createMapModel.maxLat = ""
        self.createMapModel.isMapPrivate = true
    }
    
    
    func checkValidation(arrTxtField:[UITextField],action: Selector,target:UIViewController) -> Bool
    {
        var isValidationPassed = true
        if !AppUtility.hasValidText(self.createMapModel.name)
        {
            AppUtility.addValidationErrorBtn(withErrorTxtField: arrTxtField[0], withMessage: "Please input the name of map", action: action, target: target)
            isValidationPassed = false
        }
        
        if !AppUtility.hasValidText(self.createMapModel.crs)
        {
            AppUtility.addValidationErrorBtn(withErrorTxtField: arrTxtField[1], withMessage: "Please input the crs", action: action, target: target)
            isValidationPassed = false
        }
        
        if !AppUtility.hasValidText(self.createMapModel.descp)
        {
            AppUtility.addValidationErrorBtn(withErrorTxtField: arrTxtField[2], withMessage: "Please input the description", action: action, target: target)
            isValidationPassed = false
        }
        
        if !AppUtility.hasValidText(self.createMapModel.minZoom)
        {
            AppUtility.addValidationErrorBtn(withErrorTxtField: arrTxtField[3], withMessage: "Please input the minimum zoom level", action: action, target: target)
            isValidationPassed = false
        }
        else if Int(self.createMapModel.minZoom!)! < 2 || Int(self.createMapModel.minZoom!)! > 28
        {
            AppUtility.addValidationErrorBtn(withErrorTxtField: arrTxtField[3], withMessage: "min zoom must be between 2 and 28", action: action, target: target)
            isValidationPassed = false
        }
        
        if !AppUtility.hasValidText(self.createMapModel.maxZoom)
        {
            AppUtility.addValidationErrorBtn(withErrorTxtField: arrTxtField[4], withMessage: "Please input the maximum zoom level", action: action, target: target)
            isValidationPassed = false
        }
        else if Int(self.createMapModel.maxZoom!)! < 2 || Int(self.createMapModel.maxZoom!)! > 28
        {
            AppUtility.addValidationErrorBtn(withErrorTxtField: arrTxtField[4], withMessage: "min zoom must be between 2 and 28", action: action, target: target)
            isValidationPassed = false
        }
        
        if !AppUtility.hasValidText(self.createMapModel.minLon)
        {
            AppUtility.addValidationErrorBtn(withErrorTxtField: arrTxtField[5], withMessage: "Please input the minimum longitude value", action: action, target: target)
            isValidationPassed = false
        }
        
        if !AppUtility.hasValidText(self.createMapModel.maxLon)
        {
            AppUtility.addValidationErrorBtn(withErrorTxtField: arrTxtField[6], withMessage: "Please input the maximum longitude value", action: action, target: target)
            isValidationPassed = false
        }
        
        if !AppUtility.hasValidText(self.createMapModel.minLat)
        {
            AppUtility.addValidationErrorBtn(withErrorTxtField: arrTxtField[7], withMessage: "Please input the minimum latitude value", action: action, target: target)
            isValidationPassed = false
        }
        
        if !AppUtility.hasValidText(self.createMapModel.maxLat)
        {
            AppUtility.addValidationErrorBtn(withErrorTxtField: arrTxtField[8], withMessage: "Please input the maximum latitude value", action: action, target: target)
            isValidationPassed = false
        }
        
        
        return isValidationPassed
    }
}


//MARK:- Network Apis
extension HelperGISCreateMapVC
{
    func createMap(failure:@escaping (_ errorMode:GISErrorModel?) -> Void)
    {
        var data = [String:Any]()
        data["name"] = self.createMapModel.name
        data["crs"] = self.createMapModel.crs
        data["description"] = self.createMapModel.descp
        data["minZoom"] = Int(self.createMapModel.minZoom!)
        data["maxZoom"] = Int(self.createMapModel.maxZoom!)
        data["minLon"] = Double(self.createMapModel.minLon!)
        data["maxLon"] = Double(self.createMapModel.maxLon!)
        data["minLat"] = Double(self.createMapModel.minLat!)
        data["maxLat"] = Double(self.createMapModel.maxLat!)
        data["isPublic"] = !self.createMapModel.isMapPrivate
        
        var parameter = [String:Any]()
        parameter["studio"] = ["entityName":"Map", "data":data]
        
        
        Network.asyncCallRestFullApi(baseUrl:GISFramework.gisBaseURL, pathUrl: "/service/", parameter: parameter, serviceMethod: GISServiceType.POST, success: { (json) in
            failure(nil)
        }, failure: { (error) in
            failure(error)
        }) { (isTokenRefresh) in
            
        }
        
        
    }
}
