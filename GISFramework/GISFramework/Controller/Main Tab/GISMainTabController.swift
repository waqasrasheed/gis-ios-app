//
//  GISMainTabController.swift
//  GISFramework
//
//  Created by Zuhair Hussain on 17/02/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit
import MBProgressHUD

public class GISMainTabController: UIViewController {
    
    @IBOutlet weak var topBar: GISTopBar!
    @IBOutlet weak var navHeight: NSLayoutConstraint!
    var isHidderTopBar: Bool = false;
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var collectionViewMaps: UICollectionView!
    @IBOutlet weak var collectionViewDataSets: UICollectionView!
    @IBOutlet weak var collectionViewServers: UICollectionView!
    
    var adapterMaps: GISMapListingAdapter!
    var adapterDataSets: GISDataSetAdapter!
    var adapterServers: GISServerListingAdapter!
    
    var baseURL = GISFramework.gisBaseURL! //"https://map.demo-aws.meeraspace.com"
    
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        assert(GISFramework.gisDomain.count != 0,"not called the GISFramework.configure")
        self.setTopBarGUI()
        scrollView.isScrollEnabled = false
        adapterMaps = GISMapListingAdapter(collectionViewMaps)
        adapterDataSets = GISDataSetAdapter(collectionViewDataSets)
        adapterServers = GISServerListingAdapter(collectionViewServers)
        
        
    }
    
    public override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.getMapsList()
        self.getDataSets()
        self.getMapServers()
    }
    
    public init() {
        
        let bundle = Bundle(for: type(of: self))
        super.init(nibName: "GISMainTabController", bundle: bundle)
        
        self.baseURL = GISFramework.gisBaseURL
        Network.oauthswift = GISFramework.oauthswift
        
        //self.loginInApp()
    }
    
    /*public init(baseURL:String,oauthswift: OAuth2Swift) {
     
     let bundle = Bundle(for: type(of: self))
     super.init(nibName: "TRMainTabController", bundle: bundle)
     
     self.baseURL = baseURL
     Network.oauthswift = oauthswift
     }*/
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setTopBarGUI()
    {
        if(isHidderTopBar == true)
        {
            navHeight.constant = 0;
            topBar.isHidden = true;
            return;
        }
        
        //self.topBar.lblNavHeading.text = "Oman Map"
        
        //self.topBar.btnBack.addTarget(self, action: #selector(self.btnBackPressed(_:)), for: UIControl.Event.touchUpInside)
        self.topBar.btnBack.isHidden = true
        
        //let bundle = Bundle(for: GISMapViewController.self)
        
        //self.topBar.btnRight.addTarget(self, action: #selector(self.btnMenuPressed(_:)), for: UIControl.Event.touchUpInside)
        AppUtility.setNavHeightForIPhoneX(navHightRef: self.navHeight)
        
    }
}

// MARK: - Actions
extension GISMainTabController {
    
    @IBAction func didTapBackButton(_ sender: UIButton) {
        if let navigationController = self.navigationController, navigationController.viewControllers.count > 1 {
            navigationController.popViewController(animated: true)
        } else {
            dismiss(animated: true, completion: nil)
        }
    }
    
    @IBAction func didTapMapsButton(_ sender: UIButton) {
        loadMaplist()
        
    }
    @IBAction func didTapDataSetsButton(_ sender: UIButton) {
        
        loadDataset()
    }
    @IBAction func didTapServersButton(_ sender: UIButton) {
        
        loadServer()
    }
    
    func loadMaplist() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            //self.btnMaps.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            //self.btnDataSets.setTitleColor(UIColor(red: 170 / 255, green: 170 / 255, blue: 170 / 255, alpha: 1), for: .normal)
            //self.btnServers.setTitleColor(UIColor(red: 170 / 255, green: 170 / 255, blue: 170 / 255, alpha: 1), for: .normal)
            
            self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
            
            self.topBar.lblNavHeading.text = "Maps"
        }
        
    }
    
    func loadDataset() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            
            //self.btnMaps.setTitleColor(UIColor(red: 170 / 255, green: 170 / 255, blue: 170 / 255, alpha: 1), for: .normal)
            //self.btnDataSets.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            //self.btnServers.setTitleColor(UIColor(red: 170 / 255, green: 170 / 255, blue: 170 / 255, alpha: 1), for: .normal)
            
            self.scrollView.setContentOffset(CGPoint(x: self.scrollView.frame.width, y: 0), animated: false)
            
            self.topBar.lblNavHeading.text = "Datasets"
        }
    }
    
    func loadServer() {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            
            //self.btnMaps.setTitleColor(UIColor(red: 170 / 255, green: 170 / 255, blue: 170 / 255, alpha: 1), for: .normal)
            //self.btnDataSets.setTitleColor(UIColor(red: 170 / 255, green: 170 / 255, blue: 170 / 255, alpha: 1), for: .normal)
            //self.btnServers.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            
            self.scrollView.setContentOffset(CGPoint(x: self.scrollView.frame.width * 2, y: 0), animated: false)
            self.topBar.lblNavHeading.text = "Servers"
        }
    }
}

// MARK: - Tap Handlers
extension GISMainTabController {
    
    func getMapsList(refreshCashe: Bool = false) {
        
        guard  AppUtility.hasValidText(Network.accessToken) else {
            self.loginInApp()
            return
        }
        
        if HelperGISLayer.shared.mapCashe.count > 0 && refreshCashe == false {
            
            self.adapterMaps.data = HelperGISLayer.shared.mapCashe
            return
        }
        
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        HelperGISLayer.shared.getMapsList(baseURL: self.baseURL, pathURL: "/service/map/active", completion: { (result, error) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            guard let result = result else {
                GISAlert.showServerErrors(controller: self, errorModel: error!)
                return
            }
            print(result.count)
            self.adapterMaps.data = result
            self.getActiveLayer(refreshCashe: refreshCashe)
            
        }) { (isSuccess) in
            
        }
    }
    
    func  getMapServers() {
        
        guard  AppUtility.hasValidText(Network.accessToken) else {
            self.loginInApp()
            return
        }
        
        if HelperGISLayer.shared.serverCashe.count > 0 {
            
            self.adapterServers.data = HelperGISLayer.shared.serverCashe
            return
        }
        
        HelperGISLayer.shared.getServersData(baseURL: self.baseURL, pathURL: "/service/entity/MapServer", completion: { (result, error) in
            guard let result = result else {
                GISAlert.showServerErrors(controller: self, errorModel: error!)
                return
            }
            self.adapterServers.data = result
            
        }) { (isSucces) in
            
        }
    }
    
    func getDataSets() {
        
        guard  AppUtility.hasValidText(Network.accessToken) else {
            self.loginInApp()
            return
        }
        
        if HelperGISLayer.shared.dataSetCashe.count > 0 {
            
            self.adapterDataSets.data = HelperGISLayer.shared.dataSetCashe
            
            return
        }
        
        HelperGISLayer.shared.getDataSets(baseURL: self.baseURL, pathURL: "/service/entity/MapLayer", completion: { (result, error) in
            guard let result = result else {
                GISAlert.showServerErrors(controller: self, errorModel: error!)
                return
            }
            self.adapterDataSets.data = result
            
        }) { (isSucces) in
            
        }
    }
    
    
    private func getActiveLayer(refreshCashe: Bool)
    {
        guard  AppUtility.hasValidText(Network.accessToken) else {
            return
        }
        
        if let arr = HelperGISLayer.shared.activeMap, arr.count > 0 {
            
            let result = HelperGISLayer.shared.activeMap
            let matches = self.adapterMaps.data.filter { (model) -> Bool in
                
                return result!.contains(model)
            }
            
            let _ = matches.map { (model) -> GISLayerModel in
                
                model.isActive = true
                return model
            }
            
            self.adapterMaps.reloadData()
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        HelperGISLayer.shared.getTrwslLayerData(baseURL: self.baseURL, completion: { (result, error) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            guard let error = error else {
                
                if result?.count ?? 0 > 0 {
                    
                    let matches = self.adapterMaps.data.filter { (model) -> Bool in
                        
                        return result!.contains(model)
                    }
                    
                    let _ = matches.map { (model) -> GISLayerModel in
                        
                        model.isActive = true
                        return model
                    }
                }
                
                
                self.adapterMaps.reloadData()
                return
            }
            
            GISAlert.showServerErrors(controller: self, errorModel: error)
            
        }) { (isSuccess) in
            
        }
    }
}

//MARK:- Login Method
extension GISMainTabController {
    
    //private func loginInApp(userName:String = "usman.aleem@targetofs.com", password:String = "Helpdesk@111")
    //private func loginInApp(userName:String = "targetmohsin@gmail.com", password:String = "password")
    
    private func loginInApp(userName:String = "usman.aleem@targetofs.com", password:String = "Helpdesk@111",refreshCashe: Bool = false)
    {
        
        guard  Network.oauthswift != nil else {
            self.navigationController?.popViewController(animated: true)
            return
        }
        
        MBProgressHUD.showAdded(to: self.view, animated: true)
        
        CommonNetworkApis.loginInAppToGetAccessToken(ouathInfo: Network.oauthswift, userName: userName, password: password) { (accessToken, refreshAccessToken, error) in
            
            MBProgressHUD.hide(for: self.view, animated: true)
            
            guard error == nil else {
                GISAlert.showServerErrors(controller: self, errorModel: error!)
                return
            }
            
            self.getMapsList(refreshCashe: refreshCashe)
            self.getDataSets()
            self.getMapServers()
        }
    }
}
