//
//  GISMapListingCell.swift
//  GISFramework
//
//  Created by Zuhair Hussain on 17/02/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit

class GISMapListingCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var imgTick: UIImageView!
    @IBOutlet weak var imgCover: UIImageView!
    @IBOutlet weak var iconWorld: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imgCover.layer.cornerRadius = 4
    }

}
