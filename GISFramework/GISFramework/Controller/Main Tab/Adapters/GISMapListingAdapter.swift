//
//  GISMapListingAdapter.swift
//  GISFramework
//
//  Created by Zuhair Hussain on 17/02/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

class GISMapListingAdapter: NSObject {
    
    weak var collectionView: UICollectionView!
    var bundle: Bundle!
    var parent: UIViewController? {
        return self.collectionView.viewContainingController()
    }
    var staticMap: [StaticMapModel] = []
    
    var data: [GISLayerModel]  = [] {
        didSet {
            
            staticMap = StaticMapModel.getStaticMapData()
            collectionView?.reloadData()
        }
    }
    
    
    
    func reloadData() {
        collectionView?.reloadData()
    }
    init(_ collectionView: UICollectionView) {
        super.init()
        
        self.collectionView = collectionView
        configure()
    }
    func configure() {
        bundle = Bundle(for: type(of: self))
        
        collectionView.register(UINib(nibName: "GISMapListingCell", bundle: bundle), forCellWithReuseIdentifier: "Cell")
        collectionView.dataSource = self
        collectionView.delegate = self
    }
    
}

extension GISMapListingAdapter: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return data.count + staticMap.count
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = (UIScreen.main.bounds.width - 30) / 2
        return CGSize(width: w, height: w + 54)
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.row < data.count {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GISMapListingCell
            
            let model = data[indexPath.row]
            
            cell.imgCover.image = model.mapImage.bundleImage
            cell.lblTitle.text = data[indexPath.row].name
            cell.lblDate.text = data[indexPath.row].createdDate?.dateValue(format: "yyyy-MM-dd'T'HH:mm:ss")?.stringValue(format: "dd MMM, yyyy", timeZone: .current)
            
            let worlImage = model.isActive ? "gisworld_enable" : "gisworld_disabled"
            cell.iconWorld.image = UIImage(named: worlImage, in: Bundle(for: type(of: self)), compatibleWith: nil)
            
            return cell
        }
        else {
            
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! GISMapListingCell
            
            let model = staticMap[indexPath.row - data.count]
            
            cell.imgCover.image = model.image.bundleImage
            cell.lblTitle.text = model.name
            
            return cell
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        if indexPath.row < data.count {
            
            let model = data[indexPath.row]
            
            if let layer = model.layers, layer.count > 0 {
                //let mapCtrl = GISMapViewController()
                
                self.parent?.tabBarController?.selectedIndex = 0
                let mapCtrl = self.parent?.tabBarController?.viewControllers![0] as! GISMapViewController
                
                mapCtrl.layerData = model
                mapCtrl.loadData();
                //parent?.navigationController?.pushViewController(mapCtrl, animated: true)
            }
            else {
                GISAlert.showAlert(title: "", descp: "No Layer found in \(model.name ?? "")", controller: parent!)
            }
            
        }
        else {
            
            GISAlert.showAlert(title: "", descp: "Coming Soon", controller: parent!)
            return;
            let model = staticMap[indexPath.row - data.count]
            
            //let urlStrig = GISFramework.gisBaseURL + model.url
            
            let urlStrig = "https://map.\(GISFramework.gisDomain)" + model.url
            
            let controller = GISWKWebViewController(nibName: "GISWKWebViewController", bundle: bundle)
            controller.modalPresentationStyle = .overFullScreen
            //controller.modalTransitionStyle = .crossDissolve
            controller.urlString = urlStrig
            self.parent?.present(controller, animated: true, completion: nil)
            
        }
        
        
    }
}
