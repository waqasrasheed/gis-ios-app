 //
//  GISFramework.h
//  GISFramework
//
//  Created by Waqas Rasheed on 22/02/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for GISFramework.
FOUNDATION_EXPORT double GISFrameworkVersionNumber;

//! Project version string for GISFramework.
FOUNDATION_EXPORT const unsigned char GISFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <GISFramework/PublicHeader.h>


