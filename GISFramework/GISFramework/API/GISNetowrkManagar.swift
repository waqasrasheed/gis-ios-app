//
//  GISNetowrkManagar.swift
//  GISFramework
//
//  Created by Muhammad Arslan Khalid on 14/02/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON


let Network = GISNetowrkManagar.sharedInstance
public typealias serviceCompletionerHandler = (_ result:JSON?,_ error: GISErrorModel? ) ->Void

class GISNetowrkManagar: NSObject {
    
    static let sharedInstance = GISNetowrkManagar()
    var accessToken:String!
    var refreshAccessToken:String!
    var oauthswift:OAuth2Swift!
    var isCallingRefreshToken = false
}


extension GISNetowrkManagar {
    
    func asyncCallAPIForJSONResult(WithBaseUrl baseUrl:String,WithQueryParameter queryParameter:[String:Any]?,WithDelegate delegate:UIViewController?,completion:@escaping serviceCompletionerHandler)
    {
        
        if let parameters = queryParameter
        {
            do
            {
                let data = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions.prettyPrinted)
                let strData = NSString(data: data, encoding: String.Encoding.utf8.rawValue)
                
                print("Body: \(String(describing: strData))")
                
                
            }
            catch
            {}
            
        }
        
        var headers: HTTPHeaders  = HTTPHeaders()
        
        if AppUtility.hasValidText(self.accessToken)
        {
            headers["Authorization"] = "Bearer \(self.accessToken!)"
        }
        else
        {
            //failure(GISErrorModel(errorTitle: "", errorDescp: "Token Error", error: nil))
            return
        }
        
        headers["Content-Type"] = "application/json"
        
    
        AF.request(baseUrl, method:HTTPMethod(rawValue: "POST"), parameters:queryParameter ,encoding:JSONEncoding.default,headers:headers)
            .responseString(completionHandler: { (dataStr) in
                 //print(dataStr)
                
            })
            .responseJSON { (response) in
                
                switch response.result
                {
                case .success:
                    
                    let httpResponse = response.response
                    if httpResponse?.statusCode != 200  //!=200
                    {
                        if self.isCallingRefreshToken == false
                        {
                            self.isCallingRefreshToken = true
                            //self.refreshOuth2Token(WithServiceParameter: queryParameter, completionHandler: completion, WithDelegateControlelr: delegate)
                        }
                        else
                        {
                            //self.asyncCallApiForJSONResult(WithBaseUrl: baseUrl, WithQueryParameter: queryParameter, WithDelegate: delegate, completion: completion)
                        }
                    }
                    else
                    {
                        if let res = response.value
                        {
                            
                            
                            
                            
                            let jsonVal = JSON(res)
                            let parseJson = jsonVal.rawValue as! [String:Any]
                            let graphQLError = parseJson["errors"] as? [[String:Any]]
                            
                            
                            
                            if  let _ = graphQLError
                            {
                                if (graphQLError!.count > 0)
                                {
                                    print(graphQLError![0])
                                    let msgDic = graphQLError![0]
                                    
                                    if let msg = msgDic["message"] as? String
                                    {
                                        completion(nil,GISErrorModel(errorTitle: "Maps", errorDescp: msg, error: nil, errorCode: 0, errorDic: msgDic))
                                    }
                                    else
                                    {
                                        completion(nil,GISErrorModel(errorTitle: "Map", errorDescp: "Something went wrong", error: nil, errorCode: 0,errorDic: msgDic))
                                    }
                                    
                                    //completion(nil,ErrorModel(errorTitle: "Xeena", errorDescp: "Something went wrong", error: nil, errorCode: 0,errorDic: msgDic))
                                    
                                    return;
                                }
                            }
                            
                            
                            if let _ = jsonVal.rawValue as? [String:Any]
                            {
                                DispatchQueue.main.async {
                                    completion(jsonVal,nil)
                                }
                            }
                            else
                            {
                                completion(nil,GISErrorModel(errorTitle: "Json Parsin Error", errorDescp: "Json parsing issue", error: nil, errorCode:0))
                                
                            }
                            
                        }
                        else
                        {
                            completion(nil,GISErrorModel(errorTitle: "Map", errorDescp: "Got Response Nil", error: nil))
                        }
                    }
                    
                    
                    
                    break;
                case .failure(let error):
                    
                    if  response.response?.statusCode == 401  //!=200
                    {
                        if self.isCallingRefreshToken == false
                        {
                            self.isCallingRefreshToken = true
                            //self.refreshOuth2Token(WithServiceParameter: queryParameter, completionHandler: completion, WithDelegateControlelr: delegate)
                        }
                        else
                        {
                            //self.asyncCallApiForJSONResult(WithBaseUrl: baseUrl, WithQueryParameter: queryParameter, WithDelegate: delegate, completion: completion)
                        }
                    }
                    else
                    {
                        var errorMessage = error.localizedDescription
                        
                        if errorMessage.lowercased().contains("request timed out") {
                            errorMessage = "Internet connectivity error"
                        } else if errorMessage.lowercased().contains("ssl") {
                            errorMessage = "Internet connectivity error"
                        }
                        else
                        {
                            errorMessage = "Something went wrong"
                        }
                        
                        
                        completion(nil,GISErrorModel(errorTitle: "Xeena", errorDescp: errorMessage, error: error))
                        break;
                    }
                    
                }
                
        }
        
        
    }
}
//MARK:- Restfull Api
extension GISNetowrkManagar
{
    func asyncCallRestFullApi(baseUrl:String!, pathUrl:String ,parameter:[String:Any]?,serviceMethod:GISServiceType, success:@escaping(_ json:JSON) -> Void, failure:@escaping (_ errorMode:GISErrorModel) -> Void, tokenRefresh:@escaping (_ isSuccess:Bool)->Void)
    {
        guard let urlStr = (baseUrl + pathUrl).encodeUrl
            else
        {
            failure(GISErrorModel(errorTitle: "URL Error", errorDescp: "Url is not correct", error: nil))
            return
        }
        
        var headers: HTTPHeaders  = HTTPHeaders()
        
        if AppUtility.hasValidText(self.accessToken)
        {
            headers["Authorization"] = "Bearer \(self.accessToken!)"
        }
        else
        {
            failure(GISErrorModel(errorTitle: "Xeena", errorDescp: "Token Error", error: nil))
            return
        }
        
        headers["Content-Type"] = "application/json"
        
        
        
        
        AF.request(urlStr, method:HTTPMethod(rawValue: serviceMethod.rawValue), parameters:parameter ,encoding:JSONEncoding.default,headers:headers)
            .responseString(completionHandler: { (dataStr) in
               // print(dataStr)
            })
            .responseJSON { (response) in
                
                switch response.result
                {
                case .success:
                    
//                    let str = String(data: response.data!, encoding: String.Encoding.utf8) ?? "No Data"
                    //print(str)
                    
                    let httpResponse = response.response
                    if httpResponse?.statusCode != 200  //!=200
                    {
                        if self.isCallingRefreshToken == false && httpResponse?.statusCode == 401
                        {
                            self.isCallingRefreshToken = true
                            self.refreshOuth2Token(baseUrl: baseUrl, pathUrl: pathUrl, parameter: parameter, serviceMethod: serviceMethod, success: success, failure: failure, tokenRefresh: tokenRefresh)
                        }
                        else if httpResponse?.statusCode == 500 {
                            
                            
                            failure(GISErrorModel(errorTitle: "Oman Map", errorDescp: response.description, error: nil, errorCode: 500))
                        }
                        else
                        {
                            self.asyncCallRestFullApi(baseUrl: baseUrl, pathUrl: pathUrl, parameter: parameter, serviceMethod: serviceMethod, success: success, failure: failure, tokenRefresh: tokenRefresh)
                        }
                    }
                    else
                    {
                        if let res = response.value
                        {
                            
                            let jsonVal = JSON(res)
                            
                            if jsonVal["type"].string == "FeatureCollection"
                            {
                                DispatchQueue.main.async {
                                    success(jsonVal)
                                }
                                
                                return;
                            }
                            
                            if jsonVal["success"].boolValue == true
                            {
                                DispatchQueue.main.async {
                                    success(jsonVal)
                                }
                                
                            }
                            else if jsonVal.array?.first?["x"].double != nil
                            {
                                DispatchQueue.main.async {
                                    success(jsonVal)
                                }
                            }
                                
                            else
                            {
                                if let message =  jsonVal["error"].string
                                {
                                    failure(GISErrorModel(errorTitle: "Xeena", errorDescp:message, error: nil, errorCode: jsonVal["code"].int ?? 0))
                                }
                                else if let message = jsonVal["response"].string
                                {
                                    failure(GISErrorModel(errorTitle: "Xeena", errorDescp:message, error: nil, errorCode: jsonVal["code"].int ?? 0))
                                }
                                else
                                {
                                    failure(GISErrorModel(errorTitle: "Xeena", errorDescp:"something went wrong", error: nil, errorCode: jsonVal["code"].int ?? 0))
                                }
                                
                            }
                            
                            
                            
                        }
                        else
                        {
                            failure(GISErrorModel(errorTitle: "Xeena", errorDescp: "Got Response Nil", error: nil))
                        }
                    }
                    
                    
                    
                    break;
                case .failure(let error):
                    failure(GISErrorModel(errorTitle: "Xeena", errorDescp: error.localizedDescription, error: error))
                    break;
                }
                
        }
        
    }
}


//MARK:- Refresh Access Token
extension GISNetowrkManagar
{
    func refreshOuth2Token(baseUrl:String!, pathUrl:String ,parameter:[String:Any]?,serviceMethod:GISServiceType, success:@escaping(_ json:JSON) -> Void, failure:@escaping (_ errorMode:GISErrorModel) -> Void, tokenRefresh:@escaping (_ isSuccess:Bool)->Void )
    {
        
        let refreshToken = self.refreshAccessToken!
        
        oauthswift.renewAccessToken(withRefreshToken: refreshToken) { result in
            
            switch(result)
            {
            case .success(let (_, response, _)):
                
                
                if let jsonResponce = response?.data
                {
                    do {
                        let jsonResp = try JSONSerialization.jsonObject(with: jsonResponce, options: JSONSerialization.ReadingOptions.allowFragments)
                        let parseJson = jsonResp as! [String:Any]
                        if let access_token = parseJson["access_token"] as? String
                        {
                            
                            
                            self.accessToken = access_token;
                            if let refresh_token = parseJson["refresh_token"] as? String
                            {
                                self.refreshAccessToken = refresh_token;
                            }
                            tokenRefresh(true)
                            self.asyncCallRestFullApi(baseUrl: baseUrl, pathUrl: pathUrl, parameter: parameter, serviceMethod: serviceMethod, success: success, failure: failure,tokenRefresh: tokenRefresh)
                        }
                        else
                        {
                            tokenRefresh(false)
                        }
                        
                        
                        
                    } catch(let error) {
                        tokenRefresh(false)
                        print(error.localizedDescription)
                    }
                    
                }
                
                
                self.isCallingRefreshToken = false
                break;
            case .failure(let error ):
                
                self.isCallingRefreshToken = false
                print(error.localizedDescription);
                tokenRefresh(false)
                break;
            }
            
        }
    }
}

public struct GISErrorModel {
    
    var errorTitle:String?
    var errorDescp:String?
    var error:Error?
    var errorCode:Int!
    var errorDic: [String:Any]?
    
    
    init(errorTitle:String, errorDescp:String, error:Error?, errorCode:Int = 0, errorDic: [String:Any]? = nil) {
        self.errorTitle = errorTitle
        self.errorDescp = errorDescp
        self.error = error
        self.errorCode = errorCode
        self.errorDic = errorDic
    }
}




enum GISServiceType:String {
    case POST = "POST"
    case GET = "GET"
    case PUT = "PUT"
    case DELETE = "DELETE"
}


extension String
{
    var encodeUrl:String? {
        
        if let urlStr = self.addingPercentEncoding( withAllowedCharacters: .urlQueryAllowed)
        {
            return urlStr
        }
        return nil
    }
}

//public typealias gServiceCompletionerHandler = (_ result:JSON?,_ error:GISErrorModel? ) ->Void
