//
//  GISLocationManager.swift
//  Zeena
//
//  Created by Zuhair Hussain on 23/09/2019.
//  Copyright © 2019 Target. All rights reserved.
//

import Foundation
import GoogleMaps
import CoreLocation
import Alamofire



class GISLocationManager: NSObject {
    static let shared = GISLocationManager()
    
    
    let placesGoogleURL = "https://maps.googleapis.com/maps/api/place/autocomplete/json?key=AIzaSyA_0u63uVi91MmmNohhRYPfy1udavnfEOM&input="
    let googlePlaceDetail = "https://maps.googleapis.com/maps/api/place/details/json?key=AIzaSyA_0u63uVi91MmmNohhRYPfy1udavnfEOM&placeid="
    let googleRouteURL = "https://maps.googleapis.com/maps/api/directions/json?mode=driving&key=AIzaSyA_0u63uVi91MmmNohhRYPfy1udavnfEOM"
    
    func routePointsURL(origin: CLLocationCoordinate2D, destination: CLLocationCoordinate2D) -> String? {
        let url = googleRouteURL + "&origin=\(origin.latitude),\(origin.longitude)&destination=\(destination.latitude),\(destination.longitude)"
        return url.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
    }
    
    private var currentLocation: CLLocation {
        get {
            let lat = UserDefaults.standard.double(forKey: "UserCurrentLatitude")
            let lng = UserDefaults.standard.double(forKey: "UserCurrentLongitude")
            let location = CLLocation(latitude: lat, longitude: lng)
            return location
        } set {
            UserDefaults.standard.set(newValue.coordinate.latitude, forKey: "UserCurrentLatitude")
            UserDefaults.standard.set(newValue.coordinate.longitude, forKey: "UserCurrentLongitude")
        }
    }
    
    
    
    private var locationManager = CLLocationManager()
    private var locationCallback: ((_ location: CLLocation) -> Void)?
    
    override init() {
        super.init()
        
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        
    }
    
    
    
    func getGooglePlaces(text: String, completion: @escaping (_ status: Bool, _ message: String, _ locations: [GISLocationModel]) -> Void) {
        
        let urlString = (placesGoogleURL + text + "&language=en").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        guard let url = URL(string: urlString ?? "") else { return }
        
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            
            var locations = [GISLocationModel]()
            if let result = response.value as? NSDictionary {
                if let predictions = result["predictions"] as? [NSDictionary] {
                    for p in predictions {
                        locations.append(GISLocationModel(prediction: p))
                    }
                }
            }
            
            completion(true, "", locations)
        }
        
    }
    func getPlaceDetail(placeId: String, completion: @escaping (_ status: Bool, _ message: String, _ locations: GISLocationModel) -> Void) {
        
        let urlString = (googlePlaceDetail + placeId + "&language=en").addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        guard let url = URL(string: urlString ?? "") else { return }
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            
            let location = GISLocationModel()
            
            if let result = response.value as? NSDictionary {
                if let r = result["result"] as? NSDictionary {
                    location.formattAdaddress = (r["formatted_address"] as? String) ?? ""
                    location.country = ((r["address_components"] as? [NSDictionary])?.last?["long_name"] as? String) ?? ""
                    
                    if let components = r["address_components"] as? [NSDictionary] {
                        for c in components {
                            if let types = c["types"] as? [String] {
                                if types.contains("locality") {
                                    location.city = (c["long_name"] as? String) ?? ""
                                    break
                                }
                            }
                        }
                    }
                    
                    
                    if let geometry = r["geometry"] as? NSDictionary {
                        location.latitude = ((geometry["location"] as? NSDictionary)?["lat"] as? Double) ?? 0
                        location.longitude = ((geometry["location"] as? NSDictionary)?["lng"] as? Double) ?? 0
                    }
                }
            }
            
            completion(true, "", location)
        }
    }
    func getRoutePoints(origin: CLLocationCoordinate2D, destination: CLLocationCoordinate2D, completion: @escaping (_ status: Bool, _ message: String, _ points: String) -> Void) {
        
        let urlString = routePointsURL(origin: origin, destination: destination)
        guard let url = URL(string: urlString ?? "") else {
            completion(false, "Invalid url", "")
            return
        }
        
        AF.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response) in
            
            var points = ""
            
            if let result = response.value as? NSDictionary {
                if let r = (result["routes"] as? [NSDictionary])?.first {
                    if let polyline = r["overview_polyline"] as? NSDictionary {
                        points = (polyline["points"] as? String) ?? ""
                    }
                }
            }
            
            completion(true, "", points)
        }
    }
    
    
    func reverseGeocodeCoordinate(_ coordinates:CLLocationCoordinate2D, completion: @escaping (_ location: GISLocationModel?, _ message: String) -> Void) {
        GMSGeocoder().reverseGeocodeCoordinate(coordinates) { (response, error) in
            DispatchQueue.main.async {
                if error == nil {
                    let location = GISLocationModel()
                    var address:String = ""
                    
                    guard let response = response else {
                        completion(nil, "")
                        return
                    }
                    guard let result  = response.firstResult() else {
                        completion(nil, "")
                        return
                    }
                    guard let lines  = result.lines else {
                        completion(nil, "")
                        return
                    }
                    
                    
                    switch(lines.count) {
                    case 1:
                        address = lines[0]
                        break;
                    case 2:
                        address = lines[0]
                        address =  address + ", " + lines[1]
                        break;
                    default:
                        break;
                    }
                    
                    if let data  = result.country?.trimmed {
                        if address.lowercased().contains(data.lowercased()) == false {
                            address =  address + ", " + data
                        }
                        
                    }
                    location.formattAdaddress = address
                    location.latitude = coordinates.latitude
                    location.longitude = coordinates.longitude
                    location.country = result.country ?? ""
                    location.city = result.locality ?? ""
                    
                    completion(location, "")
                } else {
                    completion(nil, error!.localizedDescription)
                }
            }
        }
    }
    
}


extension GISLocationManager: CLLocationManagerDelegate {
    func getCurrentLocation(returnPreviousIfFailed: Bool = true, _ completion: @escaping (_ location: CLLocation) -> Void) {
        locationCallback = completion
        
        if CLLocationManager.authorizationStatus() == .authorizedWhenInUse || CLLocationManager.authorizationStatus() ==  .authorizedAlways {
            locationManager.startUpdatingLocation()
        } else {
            locationManager.requestWhenInUseAuthorization()
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            locationManager.startUpdatingLocation()
        } else {
            let previousLocation = currentLocation
            locationCallback?(previousLocation)
        }
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else { return }
        manager.stopUpdatingLocation()
        currentLocation = location
        locationCallback?(location)
    }
}
