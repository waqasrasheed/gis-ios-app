//
//  GISLocationModel.swift
//  Zeena
//
//  Created by Zuhair Hussain on 23/09/2019.
//  Copyright © 2019 Target. All rights reserved.
//

import Foundation

typealias VoidCompletion = () -> Void

class GISLocationModel {
    var formattAdaddress = ""
    var latitude: Double = 0
    var longitude: Double = 0
    var country = ""
    var city = ""
    var placeId = ""
    
    init() {}
    init(prediction: NSDictionary) {
        
        formattAdaddress = prediction.object(forKey: "description") as! String
        country = (prediction.object(forKey: "terms") as? [NSDictionary])?.last?.object(forKey: "value") as? String ?? ""
        if country == "" {
            country = formattAdaddress.components(separatedBy: ",").last?.trimmed ?? ""
        }
        
        placeId = prediction.object(forKey: "place_id") as? String ?? ""
        //placeId = prediction.stringFor(key: "place_id")
    }
    init(dictionary: [String:Any]) {
        let dict = dictionary as NSDictionary
        formattAdaddress = dict.object(forKey: "formattAdaddress") as? String ?? ""
        latitude = dict.object(forKey: "latitude") as? Double ?? 0
        longitude = dict.object(forKey: "longitude") as? Double ?? 0
        country = dict.object(forKey: "country") as? String ?? ""
        city = dict.object(forKey: "city") as? String ?? ""
        placeId = dict.object(forKey: "placeId") as? String ?? ""
    }
    
    
    
    func getDicModel() -> [String:String] {
        
        var inputPram = [String:String]()
        
        inputPram["streetAddress"] = self.formattAdaddress
        inputPram["latitude"] = "\(self.latitude)"
        inputPram["longitude"] = "\(self.longitude)"
        
        return inputPram
    }
    
    var dictionary: [String:Any] {
        
        var dict = [String:Any]()
        
        dict["formattAdaddress"] = self.formattAdaddress
        dict["latitude"] = self.latitude
        dict["longitude"] = self.longitude
        dict["country"] = self.country
        dict["city"] = self.city
        dict["placeId"] = self.placeId
        
        return dict
        
    }
    
}

// MARK: - Utility Methods
extension GISLocationModel {
    func getDetail(completion: @escaping VoidCompletion) {
        GISLocationManager.shared.getPlaceDetail(placeId: placeId) { (status, message, location) in
            self.latitude = location.latitude
            self.longitude = location.longitude
            
            completion()
        }
    }
}
