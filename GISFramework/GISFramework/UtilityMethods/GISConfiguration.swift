//
//  GISConfiguration.swift
//  GISFramework
//
//  Created by Waqas Rasheed on 20/02/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

public class GISFramework {
    
    static private let scheme = "https://"
    
    static var gisDomain:String = ""
    static var gisSubDomain: String = "" //map api zeena etc
    
    static private let ssoSubDomain: String = "sso"
    static var SSO_URL: String {
        return "\(GISFramework.scheme)" + GISFramework.ssoSubDomain + "." + "\(GISFramework.gisDomain)"
    }
    
    static var oauthswift: OAuth2Swift!
    
    static public func configure(gisDomain: String, gisSubDomain: String) {
        
        GISFramework.gisDomain = gisDomain
        GISFramework.gisSubDomain = gisSubDomain
        
        GISFramework.oauthswift = OAuth2Swift(
            consumerKey: "mobile-app", //meera-dx
            consumerSecret: "ZXhhbXBsZS1hcHAtc2VjcmV1",
            authorizeUrl: GISFramework.SSO_URL + "/auth",
            accessTokenUrl: GISFramework.SSO_URL + "/token",
            responseType: "code"
        )
    }
    //    public init(domain: String, gisSubDomain: String) {
    //
    //        self.domain = domain
    //        self.gisSubDomain = gisSubDomain
    //
    //        self.oauthswift = OAuth2Swift(
    //            consumerKey: "mobile-app", //meera-dx
    //            consumerSecret: "ZXhhbXBsZS1hcHAtc2VjcmV1",
    //            authorizeUrl: SSO_URL + "/auth",
    //            accessTokenUrl: SSO_URL + "/token",
    //            responseType: "code"
    //        )
    //
    //    }
    
    static var gisBaseURL: String! {
        
        return "\(GISFramework.scheme)" + GISFramework.gisSubDomain + "." + GISFramework.gisDomain
    }
    
    static var gisProfileGraphQLURL: String! {
        
        return "\(GISFramework.scheme)" + "profile." + GISFramework.gisDomain + "/graphql"
    }
    
    
    static var imgbaseUrl = "\(GISFramework.scheme)" + "api." + GISFramework.gisDomain + "/fm/download/"
    
    
}
