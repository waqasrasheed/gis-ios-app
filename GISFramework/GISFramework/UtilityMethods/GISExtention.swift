//
//  GISExtention.swift
//  GISFramework
//
//  Created by Waqas Rasheed on 17/02/2020.
//  Copyright © 2020 Waqas Rasheed. All rights reserved.
//

import Foundation
import UIKit

extension UIColor
{
    
    static func colorWithRGB(_ red:CGFloat,_ green:CGFloat, _ blue:CGFloat, opacity:CGFloat) -> UIColor
    {
        return UIColor(red: red/255.0, green: green/255.0, blue: blue/255.0, alpha: opacity);
    }
    
    static func AppLightGreyColor() -> UIColor
    {
        return UIColor.colorWithRGB(231, 230, 231, opacity: 1.0)
    }
    
    static func AppDarkGreyColor() -> UIColor
    {
        return UIColor.colorWithRGB(139, 138, 139, opacity: 1.0)
    }
    
    static func AppTextLightGreyColor() -> UIColor
    {
        return UIColor.colorWithRGB(195, 194, 195, opacity: 1.0)
    }
    
    static var AppLightOrangeColor:UIColor {
        
        return UIColor.colorWithRGB(240, 160, 129, opacity: 1.0)
    }
    
    
    
    static var AppRedColor:UIColor {
        
        return UIColor.colorWithRGB(188, 16, 16, opacity: 1.0)
    }
    
    convenience init(hexString:String) {
        let hexString:NSString =
            hexString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) as NSString
        
        //hexString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        let scanner            = Scanner(string: hexString as String)
        
        if (hexString.hasPrefix("#")) {
            scanner.scanLocation = 1
        }
        
        var color:UInt32 = 0
        scanner.scanHexInt32(&color)
        
        let mask = 0x000000FF
        let r = Int(color >> 16) & mask
        let g = Int(color >> 8) & mask
        let b = Int(color) & mask
        
        let red   = CGFloat(r) / 255.0
        let green = CGFloat(g) / 255.0
        let blue  = CGFloat(b) / 255.0
        
        self.init(red:red, green:green, blue:blue, alpha:1)
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return NSString(format:"#%06x", rgb) as String
    }
    
    
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    static var AppOrangeColor:UIColor {
        
        return UIColor(hexString: "FF5722")
    }
    
}

extension UIViewController {
    static var name: String {
        return String(describing: self)
    }
}

extension String {
    func dateValue(format: String, timeZone: TimeZone? = TimeZone(abbreviation: "GMT")) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = timeZone
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.date(from: self)
    }
    var bundleImage: UIImage? {
        let bundle = Bundle(for: GISMapViewController.self)
        return UIImage(named: self, in: bundle, compatibleWith: nil)
    }
    
    var imageURLWithToken: URL?
    {
        
        if  AppUtility.hasValidText(self)
        {
            let urlStr = GISFramework.imgbaseUrl + self + "?access_token=\(Network.accessToken ?? "")"
            
            
            let url = URL(string: urlStr.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
            
            return url
        }
        return nil
        
    }
    
    var trimmed: String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
}


extension Date {
    
    func stringValue(format: String, timeZone: TimeZone? = TimeZone(abbreviation: "GMT")) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = timeZone
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        return dateFormatter.string(from: self)
    }
}

extension UICollectionView {
    func registerCell(ofType type: Any, withReuseIdentifier identifier: String, bundle: Bundle = Bundle.main) {
        let className = String(describing: type)
        let nib = UINib(nibName: className, bundle: bundle)
        self.register(nib, forCellWithReuseIdentifier: identifier)
    }
    func registerHeader(ofType type: Any, withReuseIdentifier identifier: String, bundle: Bundle = Bundle.main) {
        let className = String(describing: type)
        self.register(UINib(nibName: className, bundle: bundle), forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: identifier)
    }
    func addRefreshControl(_ target: Any, selector: Selector) {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(target, action: selector, for: .valueChanged)
        refreshControl.backgroundColor = UIColor.clear
        self.refreshControl = refreshControl
    }
    
}

extension UICollectionViewCell {
    static var identifier: String {
        return "k\(String(describing: Self.self))"
    }
}

extension UITableView {
    func registerCell(ofType type: Any, withReuseIdentifier identifier: String, bundle: Bundle = Bundle.main) {
        let className = String(describing: type)
        let nib = UINib(nibName: className, bundle: bundle)
        self.register(nib, forCellReuseIdentifier: identifier)
    }
    func registerHeader(ofType type: Any, withReuseIdentifier identifier: String, bundle: Bundle = Bundle.main) {
        let className = String(describing: type)
        self.register(UINib(nibName: className, bundle: bundle), forCellReuseIdentifier: identifier)
    }
    func addRefreshControl(_ target: Any, selector: Selector) {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(target, action: selector, for: .valueChanged)
        refreshControl.backgroundColor = UIColor.clear
        self.refreshControl = refreshControl
    }
    func beginRefreshing() {
        guard let refreshControl = self.refreshControl else { return }
        refreshControl.beginRefreshing()
        if self.contentOffset.y < 20 {
            self.setContentOffset(CGPoint(x: 0, y: refreshControl.frame.height), animated: true)
        }
    }
}



extension UITableViewHeaderFooterView {
    static var identifier: String {
        return "k\(String(describing: Self.self))"
    }
}
extension UITableViewCell {
    static var identifier: String {
        return "k\(String(describing: Self.self))"
    }
}


private var xoAssociationKey: UInt8 = 0
extension UIButton {
    var validationMsg:String! {
        get {
            return objc_getAssociatedObject(self, &xoAssociationKey) as? String
        }
        set(newValue) {
            objc_setAssociatedObject(self, &xoAssociationKey, newValue,  objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN)
        }
    }
}

extension UITextField {
    
    @IBInspectable var doneAccessory: Bool{
        get{
            return self.doneAccessory
        }
        set (hasDone) {
            if hasDone{
                addDoneButtonOnKeyboard()
            }
        }
    }
    
    func addDoneButtonOnKeyboard()
    {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let done: UIBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneButtonAction))
        
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        
        self.inputAccessoryView = doneToolbar
    }
    
    @objc func doneButtonAction()
    {
        self.resignFirstResponder()
    }
}

//extension NSDictionary {
//    func boolFor(key: String) -> Bool {
//        if let val = self[key] as? Bool {
//            return val
//        } else if let val = self[key] as? String {
//            return val.lowercased() == "true" || val.intValue > 0
//        } else if let val = self[key] as? Int {
//            return val > 0
//        } else if let val = self[key] as? Double {
//            return val > 0
//        }
//        return false
//    }
//    
//    func stringFor(key: String, defaultValue: String = "") -> String {
//        if let val = self[key] as? String {
//            return val
//        } else if let val = self[key] as? Int {
//            return String(val)
//        } else if let val = self[key] as? Double {
//            return String(val)
//        } else if let val = self[key] as? Bool {
//            return val == true ? "true" : "false"
//        }
//        return defaultValue
//    }
//    func doubleFor(key: String) -> Double {
//        if let val = self[key] as? Double {
//            return val
//        } else if let val = self[key] as? String {
//            return Double(val) != nil ? Double(val)! : 0
//        } else if let val = self[key] as? Int {
//            return Double(val)
//        } else if let val = self[key] as? Bool {
//            return val == true ? 1 : 0
//        }
//        return 0
//    }
//    func intFor(key: String) -> Int {
//        if let val = self[key] as? Int {
//            return val
//        } else if let val = self[key] as? String {
//            return Int(val) != nil ? Int(val)! : 0
//        } else if let val = self[key] as? Double {
//            return Int(val)
//        } else if let val = self[key] as? Bool {
//            return val == true ? 1 : 0
//        }
//        return 0
//    }
//    
//    func setDefaultFor(key: String, value: Any) {
//        if (self.allKeys as? [String])?.contains(key) == true {
//            if self[key] is NSNull {
//                self.setValue(value, forKey: key)
//            }
//        } else {
//            self.setValue(value, forKey: key)
//        }
//    }
//    
//    func makeInt(key: String) {
//        if (self.allKeys as? [String])?.contains(key) == true {
//            self.setValue(self.intFor(key: key), forKey: key)
//        } else {
//            self.setValue(0, forKey: key)
//        }
//    }
//    func makeDouble(key: String) {
//        if (self.allKeys as? [String])?.contains(key) == true {
//            self.setValue(self.doubleFor(key: key), forKey: key)
//        } else {
//            self.setValue(0.0, forKey: key)
//        }
//    }
//    func makeBool(key: String) {
//        if (self.allKeys as? [String])?.contains(key) == true {
//            self.setValue(self.boolFor(key: key), forKey: key)
//        } else {
//            self.setValue(false, forKey: key)
//        }
//    }
//    
//    func toJson() -> String {
//        if let jsonData = try?  JSONSerialization.data(withJSONObject: self, options: .prettyPrinted),
//            let jsonString = String(data: jsonData, encoding: String.Encoding.ascii) {
//            return jsonString
//        }
//        return ""
//    }
//}
